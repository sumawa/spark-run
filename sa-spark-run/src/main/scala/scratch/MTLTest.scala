package scratch

object MTLTest {

  def loexec() = {

    import cats.data.OptionT
    type ListOption[A] = OptionT[List, A]

    // create instances of ListOption using the OptionT constructor or using pure
    import cats.instances.list._
    import cats.syntax.applicative._ // for pure

    val result1: ListOption[Int] = OptionT(List(Option(10)))
    val result2: ListOption[Int] = 32.pure[ListOption]
    val result3: OptionT[List, Int] = 42.pure[ListOption] // without aliasing
    val result4: OptionT[List, Int] = 42.pure[OptionT[List, ?]] // doesn't work, why?

    // The map and flatMap methods combine the
    // corresponding methods of List and Option into single operation.
    val res = result1.flatMap { x: Int =>
      result2.map { y: Int =>
        x + y
      }
    }
    println(s"res: $res result3: $result3 and (res == result3) ??? = ${res == result3}")
  }

  /*
    Each monad transformer is a data type, defined in cats.data.
    Allows to wrap stacks of monads to product new monads
    OptionT,EitherT,ReaderT,WriterT,StateT,IdT

    Monad transformers follow same convention:

      Transformer itself represents inner monad in a stack:
        OptionT 		<==> List[Option[A]]

      First Type parameter specifies the outer monad
        OptionT[List,A] <==> List[Option[A]]

      So, we build monad stacks from the inside out:
        type ListOption[A] = OptionT[List,A]

  */
  def stackMonadExec() = {

    import cats.data.OptionT // for pure

    // Alias Either to a type constructor with one parameter:
    type ErrorOr[A] = Either[String, A]

    // Build our final monad stack using OptionT
    type ErrorOrOption[A] = OptionT[ErrorOr, A] // OptionT[Either[String,A],A]

    // ErrorOrOption is a monad, just like ListOption. We can use pure,map,flatMap

    //    val a = 10.pure[ErrorOrOption]
    //    val b = 32.pure[ErrorOrOption]
    //
    //    val c = a.flatMap(x => b.map(y => x + y))
    //    println(s"a: $a --- b: $b --- c: $c")
  }

  def threeOrMoreStackMonad() = {
    /*
        Create a Future of an Either of Option

        EitherT has three type parameters
        case class EitherT[F[_],E,A](stack: F[Either[E,A]]){
          // etc.
        }
        * F[_]  = is the outer monad in the stack (Either is the inner)
        * E 	= is the error type for the Either
        * A 	= is the result type for the Either

    */

    import cats.data.{EitherT, OptionT}

    import scala.concurrent.Future

    type FutureEither[A] = EitherT[Future, String, A]
    type FutureEitherOption[A] = OptionT[FutureEither, A]

    //    import scala.concurrent.Await
    //    import scala.concurrent.ExecutionContext.Implicits.global
    //    import scala.concurrent.duration._
    //
    //    // import cats.Monad
    //    import cats.instances.future._		// for Monad
    //    import cats.syntax.applicative._ 	// for pure
    //
    //    val futureEitherOr: FutureEitherOption[Int] =
    //      for {
    //        a <- 10.pure[FutureEitherOption]
    //        b <- 32.pure[FutureEitherOption]
    //      } yield a + b
    //
    //    println(s"futureEitherOr: ${futureEitherOr}")
  }

}
