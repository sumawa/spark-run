package scratch

object DummyScript {

  import java.nio.file.{Path, Paths}

  import cats.effect.{IO, Timer}
  import com.sa.sparkrun.conf.{DatabaseConfig, EnvConfig}
  import com.sa.sparkrun.db.{DoobieJobRepository, PooledTransactor}
  import com.sa.sparkrun.service.JobServiceImpl
  import fs2.Stream
  import pureconfig.module.catseffect.loadConfigF
  import pureconfig.{ConfigReader, Derivation}
  import pureconfig.generic.auto._

  import scala.reflect.ClassTag

  private def loadCnf[A](p: Path, ns: String)(
    implicit
    reader: Derivation[ConfigReader[A]],
    ct: ClassTag[A]
  ): Stream[IO, A] =
    Stream.eval(loadConfigF[IO, A](p, ns))

  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val T: Timer[IO] = IO.timer(global)

  import com.sa.sparkrun.params.SparkParamBuilderInstances._
  import pureconfig.module.catseffect.loadConfigF

  implicit val cs = IO.contextShift(global)

  val envConfig = loadConfigF[IO, EnvConfig](EnvConfig.namespace)


  val externalConfigPath = Paths.get("/Users/sumantawasthi/projs/scala/sa-proj/staging.conf")


  val sparkParam = paramBuilder.buildParam("/Users/sumantawasthi/projs/scala/sa-proj/staging.conf")

  val databaseConf = loadCnf[DatabaseConfig](externalConfigPath, DatabaseConfig.namespace)


  val conf = databaseConf.compile.toList.unsafeRunSync()(0)


  val xa = PooledTransactor[IO](conf).unsafeRunSync()

  val jobR = new DoobieJobRepository[IO](xa)

  val jobS = new JobServiceImpl[IO](jobR)


}
