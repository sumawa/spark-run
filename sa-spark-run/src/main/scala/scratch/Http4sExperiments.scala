package scratch

object Http4sExperiments {

  import cats.Applicative.ops.toAllApplicativeOps

  import cats.data.EitherT

  import cats.effect.{ContextShift, ExitCode, IO}

  import com.sa.sparkrun.params.StandAloneParam

  import com.sa.sparkrun.submit._

  import fs2.Stream

  import org.http4s.client.Client

  import org.http4s.client.blaze.BlazeClientBuilder

  import org.http4s.{Header, Method, Request, Uri}


  val x = Uri.uri("http://127.0.0.1:6066/v1/submissions/create")

  val req = Request[IO](Method.POST, x).withHeaders(Header("Content-Type", "application/json"))

  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val cs: ContextShift[IO] = IO.contextShift(global)

  val client = BlazeClientBuilder[IO](global)

  scala.util.Try(BlazeClientBuilder[IO](global).resource.use(_.expect[String](req)).unsafeRunSync())
  // res3: scala.util.Try[String] = Failure(java.net.ConnectException: Connection refused)

  scala.util.Try(BlazeClientBuilder[IO](global).resource.use(_.expect[String](req)).unsafeRunSync()) match {
    case scala.util.Success(x) => println(x)
    case scala.util.Failure(ex) => println(ex.getMessage); ex.printStackTrace()
  }
}
