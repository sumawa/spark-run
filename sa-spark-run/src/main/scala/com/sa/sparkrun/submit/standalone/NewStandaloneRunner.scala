package com.sa.sparkrun.submit.standalone

import cats.Applicative.ops.toAllApplicativeOps
import cats.data.EitherT
import cats.effect.{ContextShift, ExitCode, IO}
import com.sa.sparkrun.params.StandAloneParam
import com.sa.sparkrun.submit._
import fs2.Stream
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.{Header, Method, Request, Uri}

object NewStandaloneRunner{
  def makeRunner[F[_]] = new NewStandaloneRunner[F]()
}


class NewStandaloneRunner[F[_]] extends SparkRunner[F]{

  import cats.effect.LiftIO
  type EitherSP[A] = EitherT[F,String,A]
  type ES1[SparkResponse] = EitherT[IO,String,SpSuccess]

//  implicit def myEffectLiftIO: LiftIO[EitherSP] =
//    new LiftIO[EitherSP] {
//      override def liftIO[A](ioa: IO[A]): EitherSP[A] = {
//        ioa.attempt.unsafeRunSync() match {
//          case Left(ex) => EitherSP[A](Left(ex.getMessage))
//        }
//      }
//    }

  //  implicit def myEffectLiftIO: LiftIO[MyEffect] =
//    new LiftIO[MyEffect] {
//      override def liftIO[A](ioa: IO[A]): MyEffect[A] = {
//        ioa.attempt.unsafeToFuture()
//      }
//    }
//    type EitherSP[A] = EitherT[IO,String,A]

  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs:ContextShift[IO] = IO.contextShift(global)

  def submit(client: Client[IO], sparkParam: StandAloneParam, str: String): IO[Either[String,SpSuccess]] = {
    import io.circe.generic.auto._
    //    import io.circe.parser.decode
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityEncoder._

    val dsu = Uri.fromString(str)

    println(s"######## LAUNCHING NEW STANDALONE RUNNER SPARK SUBMIT ######## :: ${System.currentTimeMillis()}")
    //    println(sparkParam.asJson)

    import org.http4s.circe.CirceEntityDecoder._

    dsu match {
      case Right(x) =>
        val req = Request[IO](Method.POST, x)
          .withHeaders(Header("Content-Type","application/json"))
          .withEntity(sparkParam.asJson)


          import scala.util.{Try,Success,Failure}
        val res: IO[Either[String,SpSuccess]] = Try(client.expect[SpResponse](req).unsafeRunSync()) match {
          case Success(spResponse) =>
            println(s"NEWSPARKRUNNER: SUCCESS x ${spResponse}")
            val out = SpSuccess(spResponse.submissionId,AppStatus.Submitted)
            IO{
              println(s"NEWSPARKRUNNER SUCCESS RESPONSE: ${out}")
              Right(out)
            }

          case Failure(ex) =>
            println(s"NEWSPARKRUNNER: FAILURE ex ${ex.printStackTrace()}")
            IO(Left(s"CONNECTION PROBLEM: ${ex.getMessage}"))
        }
        res

      case Left(ex) =>
        println(s"NEWSPARKRUNNER: BADURI  ${ex.printStackTrace()}")
        IO(Left(s"BAD URI: ${ex.getMessage()}"))
//        throw  new Exception(s"BAD URI: ${ex.printStackTrace()}")

    }

  }

  override def executeRunner(): F[SparkResponse] = ???

//  override def executeRunner(str: String, sparkParam: StandAloneParam): IO[SparkResponse] = {
//    //    println(s"########## LAUNCHING STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
//    val io = (for {
//      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
//    } yield strm).compile.toList.map(_.head).unsafeRunSync()
//    IO.pure(io)
//  }

  def run(str: String, sparkParam: StandAloneParam): IO[Either[String,SpSuccess]] = {
    println(s"########## NEWSPARKRUNNER::: STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
    val io = (for {
      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
    } yield strm).compile.toList.map(_.head).unsafeRunSync()
    IO.pure(io)
  }


//  def run(str: String, sparkConfPath: String): IO[SparkResponse] = {
//    //    println(s"########## LAUNCHING STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
//    val io = (for {
//      sparkParam <- paramBuilder.buildParam(sparkConfPath)
//      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
//    } yield strm).compile.toList.map(_.head).unsafeRunSync()
//    IO.pure(io)
//  }

}
