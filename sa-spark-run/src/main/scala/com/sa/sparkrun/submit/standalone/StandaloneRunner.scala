package com.sa.sparkrun.submit.standalone

import cats.effect.{ContextShift, ExitCode, IO}
import com.sa.sparkrun.params.{SparkParam, StandAloneParam}
import com.sa.sparkrun.submit.{AppStatus, SpResponse, SpSuccess, SparkResponse, SparkRunner}
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.{Header, Method, Request, Uri}
import cats.Applicative.ops.toAllApplicativeOps
import com.sa.sparkrun.params.SparkParamBuilderInstances.paramBuilder
import fs2.Stream

object StandaloneRunner{
  def makeRunner[F[_]] = new StandaloneRunner[F]()
}
class StandaloneRunner[F[_]] extends SparkRunner[F]{

  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs:ContextShift[IO] = IO.contextShift(global)

  def submit(client: Client[IO], sparkParam: StandAloneParam, str: String): IO[SparkResponse] = {
    import io.circe.generic.auto._
    //    import io.circe.parser.decode
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityEncoder._

    val dsu = Uri.fromString(str)

    println(s"######## LAUNCHING STANDALONE SPARK SUBMIT ######## :: ${System.currentTimeMillis()}")
    //    println(sparkParam.asJson)

    import org.http4s.circe.CirceEntityDecoder._

    dsu match {
      case Right(x) =>
        val req = Request[IO](Method.POST, x)
          .withHeaders(Header("Content-Type","application/json"))
          .withEntity(sparkParam.asJson)
        import cats.effect.Blocker
        val responseBody = Blocker[IO].use { blocker =>

          def execute = {
            val res = client.expect[SpResponse](req)
//            val res = BlazeClientBuilder[IO](global).resource.use(_.expect[SpResponse](req))
            println(s"SPARK RUNNER : res ${res}")
            res.map(println)
            res.flatMap(resp => IO(println(resp))).as(ExitCode.Success)
            val out = res.map(spResponse =>SpSuccess(spResponse.submissionId,AppStatus.Submitted))
            IO{
              val ores = out.unsafeRunSync()
              println(s"SPARK RUNNER ORES: ${ores}")
              ores
            }
          }
          blocker.blockOn(execute)
        }
        responseBody
//        responseBody.map(println)
//        responseBody.flatMap(resp => IO(println(resp))).as(ExitCode.Success)
//        responseBody.map(spResponse =>SpSuccess(spResponse.submissionId,AppStatus.Submitted))
      case Left(ex) => throw  new Exception(s"BAD URI: ${ex.printStackTrace()}")
    }

  }

  override def executeRunner(): F[SparkResponse] = ???

//  override def executeRunner(str: String, sparkParam: StandAloneParam): IO[SparkResponse] = {
//    //    println(s"########## LAUNCHING STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
//    val io = (for {
//      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
//    } yield strm).compile.toList.map(_.head).unsafeRunSync()
//    IO.pure(io)
//  }

  def run(str: String, sparkParam: StandAloneParam): IO[SparkResponse] = {
    println(s"########## SPARK RUNNER ::: STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
    val io = (for {
      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
    } yield strm).compile.toList.map(_.head).unsafeRunSync()
    IO.pure(io)
  }


//  def run(str: String, sparkConfPath: String): IO[SparkResponse] = {
//    //    println(s"########## LAUNCHING STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
//    val io = (for {
//      sparkParam <- paramBuilder.buildParam(sparkConfPath)
//      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
//    } yield strm).compile.toList.map(_.head).unsafeRunSync()
//    IO.pure(io)
//  }

}
