//package com.sa.sparkrun.submit
//
//import cats.effect.{Effect, IO}
////import com.sa.sparkrun.conf.SparkConf
//import com.sa.sparkrun.params.SparkParam
//import com.sa.sparkrun.submit.{SubmitResult, Submitter}
//import org.http4s.{Method, Request, Response, Uri}
//import io.chrisdavenport.log4cats.StructuredLogger
////import org.apache.hadoop.conf.Configuration
////import org.apache.hadoop.yarn.api.records.{
////  ContainerLaunchContext,
////  LocalResource,
////  Resource => YarnResource
////}
////import org.apache.hadoop.yarn.client.api.{YarnClient, YarnClientApplication}
////import org.apache.hadoop.yarn.util.Records
//
//object TestSubmitter {
//  def make[F[_]](
//                  //                  client: YarnClient,
//                  sparkParam: SparkParam,
//                  logger: StructuredLogger[F]
//                )(implicit F: Effect[F]): Submitter[F] =
//    new TestSubmitter(
////      client,
//      logger,
//      sparkParam
////      new LocalResourceFactory[F](logger, hadoopConf),
////      new ArgFactory(conf),
////      new EnvFactory(conf)
//    )
//}
//
//class TestSubmitter[F[_]](
//                           logger: StructuredLogger[F],
//                            sparkParam: SparkParam,
//                         )(
//                           implicit
//                           F: Effect[F]
//                         ) extends Submitter[F] {
//  import cats.implicits._
//
//
////  private def createApplication(client: YarnClient) =
////    F.delay {
////      val app = client.createApplication
////      if (app == null) {
////        throw new NullPointerException(
////          "client.createApplication returned null with no particular problem"
////        )
////      }
////      app
////    }
//
//  import scala.collection.JavaConverters._
//
//  import org.http4s.dsl.io._
//  import org.http4s.implicits._
//  import org.http4s.circe
//  import io.circe.syntax._
//  import io.circe.generic.auto._
//  import cats.effect.{IO,LiftIO}
//
//  println(s"######## LAUNCHING SPARK SUBMIT ######## :: ${System.currentTimeMillis()}")
//
//  import org.http4s.circe.CirceEntityEncoder._
//  import org.http4s.Header
//  import cats.effect._
//  import org.http4s.client.blaze.BlazeClientBuilder
//  import scala.concurrent.ExecutionContext.Implicits.global
//
////  private def doSubmission(app: String)(implicit contextShift: ContextShift[IO]): String =
//  private def doSubmission(app: String)(implicit contextShift: ContextShift[IO]): IO[String] =
//
//    for {
//      _ <- logger.info("preparing to launch")
//      req = Request[IO](Method.POST, Uri.uri("http://127.0.0.1:6066/v1/submissions/create"))
//        .withHeaders(Header("Content-Type", "application/json;charset=UTF-8"))
//        .withEntity(sparkParam.asJson)
////      res <- processResponse(req)
////      responseBody = BlazeClientBuilder[IO](global).resource.use(_.expect[String](req))
////      res <- responseBody
////      println (s"RES: ${res}")
//    } yield req
//
//  private def processResponse(req: Request[IO])(implicit contextShift: ContextShift[IO])=
//    for {
//      res <- BlazeClientBuilder[IO](global).resource.use(_.expect[String](req))
//    } yield res
////      responseBody = BlazeClientBuilder[IO](global).resource.use(_.expect[String](req)
////      responseBody.flatMap(resp => IO(println(resp))).as(ExitCode.Success)
//
////      val request: Request[IO] = Request[IO](Method.POST, Uri.unsafefromString("http://localhost:8888/hello")).withEntity(Person("Jon", Some(18), List("1111")))
////      val response: IO[Response[IO]] = client.expect[String](IO(request))
////      val client = HttpClient
////      val request: Request[IO] = Request[IO](Method.POST
////          , Uri.unsafefromString("http://localhost:8888/hello")).withEntity(Person("Jon", Some(18), List("1111")))
////      val response: IO[Response[IO]] = client.expect[String](IO(request))
////      val body = json"""{"hello":"world"}"""
////      val req = {println(s"launchign request");Request[IO](Method.POST, Uri.uri("http://127.0.0.1:6066/v1/submissions/create"))
////        .withBody(app)
////        .unsafeRunSync()}
//
////    } yield appId
//
////  private def processSubmission(app: String) =
////    for {
////      _ <- logger.info(s"submitting app: String")
////      appId <-
////    } yield appId
//
////  private def run(app: Application) =
////    for {
////      _            <- logger.info(s"running app: ${app.toString}")
////      appContainer <- createApplication(yarnClient) // create an application master
////      appResponse  = appContainer.getNewApplicationResponse
////      _            <- logger.info(s"container is created: #${appResponse.getApplicationId}")
////      maxMem       = appResponse.getMaximumResourceCapability.getMemory // check for resources
////      _            <- logger.info(s"memory allowed: $maxMem")
////      appId        <- doSubmission(yarnClient, appContainer, app) // submit spark app
////    } yield appId
//
//  import cats.implicits._
//
//  def submit(app: String)(implicit contextShift: ContextShift[IO]): F[SubmitResult] =
////def submit(app: String)(implicit contextShift: ContextShift[IO]): IO[String] =
//    doSubmission(app).attemptT
//      .foldF(
//        e =>
//          logger.error(e)(s"can't create an application: ${e.getMessage}") *>
//            F.pure(SubmitResult.failure(e)),
//        appId =>
//          logger.info(s"successfully submitted: $appId") *>
//            F.pure(SubmitResult.success(appId, AppStatus.Submitted.toString))
//      ) // for now
//
////  private def createResourceMap(app: Application): F[Map[String, LocalResource]] =
////    localResourceFactory
////      .fromPath(app.jarPath)
////      .map { res =>
////        Map(
////          "__app__.jar" -> res
////        )
////      }
////
////  private def bindResource(app: Application) = {
////    val resource = Records.newRecord(classOf[YarnResource])
////    resource.setMemory(app.resources.memory)
////    resource.setVirtualCores(app.resources.cores)
////
////    resource
////  }
//
////  object UserName extends QueryParamDecoderMatcher[String]("user_name")
////
////  object Age extends OptionalQueryParamDecoderMatcher[Int]("age")
////
////  object PhoneNumber extends OptionalMultiQueryParamDecoderMatcher[String]("phone")
////
////  case class Person(name: String, age: Option[Int], phoneNumbers: List[String])
////
////  object Person {
////    implicit val personDecoder: Decoder[Person] = new Decoder[Person] {
////      override def apply(c: HCursor): Decoder.Result[Person] =
////        for {
////          name <- c.get[String]("name")
////          age <- c.get[Option[Int]]("age")
////          phoneNumbers <- c.get[List[String]]("phone")
////        } yield Person(name, age, phoneNumbers)
////    }
////
////    implicit val personEncoder: Encoder[Person] = new Encoder[Person] {
////      override def apply(a: Person): Json = Json.obj(
////        "name" -> a.name.asJson,
////        "age" -> a.age.asJson,
////        "phone" -> a.phoneNumbers.asJson
////      )
////    }
////
////    implicit val personEntityDecoder: EntityDecoder[IO, Person] = jsonOf[IO, Person]
////    implicit val personEntityEncoder: EntityEncoder[IO, Person] = jsonEncoderOf[IO, Person]
////
////  }
//
////  def helloWorldRoutes: HttpRoutes[IO] =
////    HttpRoutes
////      .of[IO]({
////        case GET -> Root / "hello" / name => Ok(s"Hello, ${name}")
////        case GET -> Root / "hello" :? UserName(name) +& Age(age) +& PhoneNumber(phoneNumber) =>
////          phoneNumber match {
////            case Invalid(e) => BadRequest("Failed to parse phone number.")
////            case Valid(Nil) =>
////              age.fold(Ok(s"Hello, ${name}"))(x => Ok(s"Hello, ${name}, your age is ${x}"))
////            case Valid(numbers) =>
////              age.fold(Ok(s"Hello, ${name}, your phone number is ${numbers.mkString(";")}"))(
////                x => Ok(s"Hello, ${name}, your age is ${x}, your phone number is ${numbers.mkString(";")}")
////              )
////          }
////        case GET -> Root / "json" / "hello" :? UserName(name) +& Age(age) +& PhoneNumber(phoneNumber) =>
////          phoneNumber match {
////            case Invalid(e) => BadRequest("Failed to parse phone number.")
////            case Valid(numbers) =>
////              Ok(Person(name, age, numbers))
////          }
////        case request @ POST -> Root / "json" / "hello" =>
////          request.as[Person].flatMap(person => Ok(person))
////        case GET -> Root / "add" / "int" / IntVar(x) / IntVar(y)    => Ok(s"Result is ${x + y}")
////        case GET -> Root / "add" / "long" / LongVar(x) / LongVar(y) => Ok(s"Result is ${x + y}")
////        case request @ GET -> Root / "header"                       => Ok(request.headers.toString)
////      })
////  override def run(args: List[String]): IO[ExitCode] = {
////    val resource = for {
////      client <- BlazeClientBuilder[IO](scala.concurrent.ExecutionContext.Implicits.global).resource
////      server <- BlazeServerBuilder[IO]
////        .bindHttp(8888, "0.0.0.0")
////        .withHttpApp((helloWorldRoutes <+> Employee.routes(client)).orNotFound)
////        .resource
////    } yield server
////    resource.use(_ => IO.never)
////  }
//}
