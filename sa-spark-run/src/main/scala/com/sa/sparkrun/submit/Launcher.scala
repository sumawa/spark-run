package com.sa.sparkrun.submit

import cats.Parallel
import cats.effect.{Blocker, ContextShift, Effect, IO}
import com.sa.sparkrun.db.domain.job.Job
import com.sa.sparkrun.service.{JobService, JobServiceImpl}
import io.chrisdavenport.log4cats.StructuredLogger
import cats.Applicative.ops.toAllApplicativeOps
import com.sa.sparkrun.db.DoobieJobRepository
import com.sa.sparkrun.db.models.ExecutionJobStatus
import com.sa.sparkrun.params.{SparkParam, StandAloneParam}

class Launcher[F[_]](
                      sparkSubmitUrl: String,
                      sparkParam: StandAloneParam,
                      logger: StructuredLogger[F]
                      , blocker: Blocker
                    )(
                      implicit
                      F: Effect[F],
                      p: Parallel[F],
                      cs: ContextShift[F]
                    ) {


  // updates with status given by YARN
//  private def updateJob(job: Job, appId: String, status: AppStatus): F[Unit] = {
//    println(s"UPDATE JOB : ${job.id} and is Submitted? ${status == AppStatus.Submitted}")
//    status match {
//      // YARN may return one of these statuses if submitted job was queued
//      case AppStatus.New | AppStatus.NewSaving | AppStatus.Accepted | AppStatus.Submitted =>
////        jobS.setSubmitted(job.id, appId)
//        println(s"UPDATE JOB : FOUND STATUS = ${status} so setting JOB : ${job.id}  With appId: ${appId} and Submitted? ${ExecutionJobStatus.Submitted}")
//        IO(jobS.setAppIdOnly(job.id, appId)).unsafeRunSync()
////        for {
////          b <- jobS.setStatusOnly(job.id, ExecutionJobStatus.Submitted)
//////          _ <- IO(println(s"job submitted : ${b}"))
////        } yield b
//        val b = jobS.setStatusOnly(job.id, ExecutionJobStatus.Submitted)
//        b.map(println)
////        IO(jobS.setStatusOnly(job.id, ExecutionJobStatus.Submitted)).unsafeRunSync().void
//
////        blocker.blockOn(updateStatus)
////        def updateStatus = {
////          jobS.setAppIdOnly(job.id, appId)
////          jobS.setStatusOnly(job.id, ExecutionJobStatus.Submitted)
////        }
//      // but if the queue was empty it's possible the job would be put to Running state
//      case AppStatus.Running =>
//        jobS.setRunning(job.id, appId)
//      case _ =>
//        // this is just for the first phases, this behaviour is unexpected now
//        // but might be very real for YARN
//        logger.info("SPARK finished the task during submitting") *>
//          F.raiseError(new RuntimeException("unexpected status"))
//    }
//  }


  import com.sa.sparkrun.submit.standalone.NewStandaloneRunner
  private def process(job: Job): IO[Either[String,SpSuccess]] = {
    implicit val cs = IO.contextShift(scala.concurrent.ExecutionContext.Implicits.global)
    for {
      sparkResponse <- blocker.blockOn(NewStandaloneRunner.makeRunner[IO].run(sparkSubmitUrl,sparkParam))
      //      sparkResponse <- blocker.blockOn(NewStandaloneRunner.makeRunner[IO].run(sparkSubmitUrl,sparkParam))
    } yield (sparkResponse)
  }

  def run(jobR: DoobieJobRepository[IO]): F[Unit] = {
    println(s"LAUNCHER RUN 4444444")
    import fs2.Stream
    val lst = for {
      _ <- IO(println(s"GOT 4444444 DoobieJobRepository: ${jobR}"))
      jobS1           = new JobServiceImpl[IO](jobR)
      jl = jobS1.allQueued.unsafeRunSync()
      _ <- IO(println(s"GOT 4444444 QUEUED JOBS: ${jl.size}"))

      res = jl.map { j => handleResponse(j) }

      r =  res.map { tuple =>
        jobS1.setAppIdOnly(tuple._1, tuple._2).unsafeRunSync()
        jobS1.setStatusOnly(tuple._1, ExecutionJobStatus.Submitted).unsafeRunSync()
      }
      //  ONLY FOR TESTING
      l = jobS1.insert(Job(0,java.util.UUID.randomUUID().toString,"HEHE")).unsafeRunSync()

    } yield ()
    lst.unsafeRunSync()
    F.unit
  }

  def handleResponse(job: Job): (Int,String,AppStatus) = {
    val sp = process(job).unsafeRunSync()
    sp match {
      case Right(SpSuccess(appId: String, status: AppStatus)) => (job.id, appId, status)
      case Left(err) =>
        println(s"ERROR 4444444: ${err}")
        throw new Exception(err)
    }

  }

}

////  //    jobS.allQueued
////  //      .flatMap(jSeq => logger.info(s"received ${jSeq.length} job instances") *> F.pure(jSeq))
////  //      .flatMap(_.map(process).parSequence)
////  //      .void

