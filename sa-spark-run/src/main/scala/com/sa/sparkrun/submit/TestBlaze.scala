package com.sa.sparkrun.submit

import cats.data.Chain
import cats.effect._
import org.http4s._
import org.http4s.client.blaze.BlazeClientBuilder
//import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.io._
import org.http4s.Uri.uri

import scala.concurrent.ExecutionContext.Implicits.global
import cats.effect.IO
import cats.implicits._

//https://github.com/http4s/http4s/blob/main/examples/blaze/src/main/scala/com/example/http4s/blaze/ClientPostExample.scala

//object TestBlaze extends IOApp with Http4sClientDsl[IO] {
object TestBlaze extends IOApp  {
  import io.circe.syntax._
  import io.circe.generic.auto._

//  import com.sa.sparkrun.conf.{EnvVar, SparkConf}
  import io.circe.parser.decode

  case class EnvVar(SP: Option[String] = Some("1"), SPARK_USER: Option[String] = Some("sumantawasthi"))

  case class SparkConf(action: String,
                       appResource: String,
                       mainClass: String,
                       sparkProperties: Map[String, String],
                       clientSparkVersion: String = "2.4.0",
                       appArgs: List[String] ,
                       environmentVariables: EnvVar)

  object SparkConf {
    val namespace: String = "sparkConf"
  }

  val sc = SparkConf(
    "CreateSubmissionRequest"
    , "/Users/sumantawasthi/tools/bigdata/spark-2.4.0-bin-hadoop2.7/examples/jars/spark-examples_2.11-2.4.0.jar"
    , "org.apache.spark.examples.SparkPi"
    , Map[String,String](
      "spark.executor.memory" -> "2g"
      , "spark.master" -> "spark://127.0.0.1:7077"
      , "spark.eventLog.enabled" ->  "false"
      , "spark.app.name" -> "Spark REST API - PI"
      , "spark.jars" -> "/Users/sumantawasthi/tools/bigdata/spark-2.4.0-bin-hadoop2.7/examples/jars/spark-examples_2.11-2.4.0.jar"
      , "spark.driver.supervise" -> "true"
    )
    , "2.4.0"
    , List[String]("80")
    , EnvVar(Some("1"), Some("sumantawasthi"))
  )



  val encSc = sc.asJson
  val decSC = decode[SparkConf](encSc.toString)

  case class SC(abc: String)
  val scJson = SC("ABC").asJson
  decode[SC](scJson.toString)


  def run(args: List[String]): IO[ExitCode] = {

    import org.http4s.circe.CirceEntityEncoder._

    println(sc)
    println(sc.asJson)
    val req = Request[IO](Method.POST, Uri.uri("http://127.0.0.1:6066/v1/submissions/create"))
      .withHeaders(Header("Content-Type","application/json"))
      .withEntity(sc.asJson)
//      .withBody(sc.asJson)

    val responseBody = BlazeClientBuilder[IO](global).resource.use(_.expect[String](req))
    responseBody.flatMap(resp => IO(println(resp))).as(ExitCode.Success)

  }

  def testGet(url: String) = {

//    import cats.effect.{IO,ContextShift}
//    import org.http4s._
//    import org.http4s.client.blaze.BlazeClientBuilder
//    import org.http4s.client.dsl.Http4sClientDsl
//    import org.http4s.dsl.io._
//    import org.http4s.Uri.uri
//
//    // import scala.concurrent.ExecutionContext.Implicits.global
//    import cats.effect.IO
//    import cats.implicits._
//
//    import io.circe.syntax._
//    import io.circe.generic.auto._
//
//    import com.sa.sparkrun.conf.{EnvVar, SparkConf}
//    import io.circe.parser.decode
//    import scala.concurrent.ExecutionContext
//
//
//    implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)
//    import cats.Applicative.ops.toAllApplicativeOps
//
//    val req = Request[IO](Method.GET, Uri.uri("http://127.0.0.1:6066/v1/submissions/status/driver-20201116085227-0005"))
//
//
//    import org.http4s.client.blaze.BlazeClientBuilder
//
//    import cats.Applicative.ops.toAllApplicativeOps
//    import scala.concurrent.ExecutionContext.Implicits.global
//
//
//
//    val responseBody = BlazeClientBuilder[IO](global).resource.use(_.expect[String](req))
//    val res = responseBody.flatMap(resp => IO(println(resp)))
//    res.unsafeRunSync()






    println("Launched MONITOR ")
//    F.pure(SubmitResult.success("APPID",AppStatus.Submitted.toString))

  }
}


//curl -X POST http://127.0.0.1:6066/v1/submissions/create --header "Content-Type:application/json;charset=UTF-8" --data '{
//"appResource": "/Users/sumantawasthi/tools/bigdata/spark-2.4.0-bin-hadoop2.7/examples/jars/spark-examples_2.11-2.4.0.jar",
//"sparkProperties": {
//"spark.executor.memory": "1g",
//"spark.master": "spark://127.0.0.1:7077",
//"spark.driver.memory": "1g",
//"spark.driver.cores": "2",
//"spark.eventLog.enabled": "false",
//"spark.app.name": "Spark REST API - PI",
//"spark.submit.deployMode": "cluster",
//"spark.jars": "file:///Users/sumantawasthi/tools/bigdata/spark-2.4.0-bin-hadoop2.7/examples/jars/spark-examples_2.11-2.4.0.jar",
//"spark.driver.supervise": "false"
//},
//"clientSparkVersion": "2.4.0",
//"mainClass": "org.apache.spark.examples.SparkPi",
//"environmentVariables": {
//"SPARK_ENV_LOADED": "1"
//},
//"action": "CreateSubmissionRequest",
//"appArgs": [
//"80"
//]
//}'