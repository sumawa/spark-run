package com.sa.sparkrun.submit

import java.nio.file.Path
import java.util.UUID

import cats.effect.{Effect, Timer}
import com.sa.sparkrun.params.SparkParam
import com.sa.sparkrun.submit.standalone.StandaloneRunner

//sealed trait SparkResponse[+A] {
//  sr =>
//  def fold[A](onError: Throwable => A, onSuccess: (String, AppStatus) => A): A = {
//    println(s"fold !!!!!! ${onSuccess}")
//    sr match {
//      case SpSuccess(appId: String, status: AppStatus) => onSuccess(appId, status)
//      case SpFailure(err)                             => onError(err)
//    }
//  }
//}
//
//case class SpSuccess(appId: String, status: AppStatus)   extends SparkResponse[String]
//case class SpFailure(err: Throwable)                  extends SparkResponse[String]
//
//object SparkResponse {
//  def success(appId: String, status: AppStatus): SparkResponse[String] = SpSuccess(appId, status)
//  def failure(exception: Throwable): SparkResponse[String]            = SpFailure(exception)
//}

sealed trait SparkResponse {
  sr =>
  def fold[A](onError: Throwable => A, onSuccess: (String, AppStatus) => A): A = {
    println(s"fold !!!!!! ${onSuccess}")
    sr match {
      case SpSuccess(appId: String, status: AppStatus) => onSuccess(appId, status)
      case SpFailure(err)                             => onError(err)
    }
  }
}

case class SpSuccess(appId: String, status: AppStatus)   extends SparkResponse
case class SpFailure(err: Throwable)                  extends SparkResponse

object SparkResponse {
  def success(appId: String, status: AppStatus): SparkResponse = SpSuccess(appId, status)
  def failure(exception: Throwable): SparkResponse            = SpFailure(exception)
}


import cats.effect.IO
import fs2.Stream

trait SparkRunner[F[_]]{
  def executeRunner(): F[SparkResponse]
}

object SparkRunnerInstances {
//  implicit val sparkRunner = StandaloneRunner.makeRunner()
}
//import cats.effect.{IO}
//trait Submitter[F[_]] {
//  def submit(app: String): IO[String]
//}

//class DummySubmitter[F[_]](implicit F: Effect[F], timer: Timer[F]) extends Submitter[F] {
//  import cats.implicits._
//
//  import scala.concurrent.duration._
//
//  def submit(app: Application): F[SubmitResult] =
//    timer.sleep(math.abs(scala.util.Random.nextInt(3)).seconds) *>
//      F.pure(SubmitResult.success(UUID.randomUUID.toString, AppStatus.Running))
//}
