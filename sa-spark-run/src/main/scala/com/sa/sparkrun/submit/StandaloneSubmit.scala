package com.sa.sparkrun.submit

import cats.effect.{ContextShift, ExitCode, IO}
import com.sa.sparkrun.params.{SparkParam, StandAloneParam}
import org.http4s.{Method, Request, Uri}
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import com.sa.sparkrun.params.SparkParamBuilderInstances._
import org.http4s.Header
import org.http4s.client.blaze.BlazeClientBuilder
import cats.Applicative.ops.toAllApplicativeOps
import fs2.Stream

class StandaloneSubmit[F[_]]  {
//class StandaloneSubmit {
  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs:ContextShift[IO] = IO.contextShift(global)

  def submit(client: Client[IO], sparkParam: StandAloneParam, str: String): IO[SpResponse] = {
    import io.circe.generic.auto._
//    import io.circe.parser.decode
    import io.circe.syntax._
    import org.http4s.circe.CirceEntityEncoder._

    val dsu = Uri.fromString(str)

    println(s"######## LAUNCHING STANDALONE SPARK SUBMIT ######## :: ${System.currentTimeMillis()}")
//    println(sparkParam.asJson)

    import org.http4s.circe.CirceEntityDecoder._

    dsu match {
      case Right(x) =>
        val req = Request[IO](Method.POST, x)
          .withHeaders(Header("Content-Type","application/json"))
          .withEntity(sparkParam.asJson)
        import cats.effect.Blocker
        val responseBody = Blocker[IO].use { blocker =>
          blocker.blockOn( BlazeClientBuilder[IO](global).resource.use(_.expect[SpResponse](req)) )
        }
        responseBody.map(println)
        responseBody.flatMap(resp => IO(println(resp))).as(ExitCode.Success)
        responseBody
      case Left(ex) => throw  new Exception(s"BAD URI: ${ex.printStackTrace()}")
    }

  }

  def run(str: String, sparkConfPath: String): IO[SpResponse] = {
//    println(s"########## LAUNCHING STANDALONE SUBMIT ########## ${System.currentTimeMillis()}")
    val io = (for {
      sparkParam <- paramBuilder.buildParam(sparkConfPath)
      strm <- Stream.eval(BlazeClientBuilder[IO](global).resource.use(submit(_,sparkParam, str)))
    } yield strm).compile.toList.map(_.head).unsafeRunSync()
    IO.pure(io)
  }

}
