package com.sa.sparkrun.params

import cats.effect.{IO}
import fs2.Stream

import pureconfig.generic.auto._

sealed trait SparkParam[+A]
case class EnvVar(SP: Option[String] = Some("1"), SPARK_USER: Option[String] = Some("sumantawasthi"))

case class StandAloneParam(action: String,
                     appResource: String,
                     mainClass: String,
                     sparkProperties: Map[String, String],
                     clientSparkVersion: String = "2.4.0",
                     appArgs: List[String] ,
                     environmentVariables: EnvVar = EnvVar(Some("1"), Some("sumantawasthi"))) extends SparkParam[StandAloneParam]

object StandAloneParam {
  val namespace: String = "standAloneParam"
}

trait SparkParamBuilder[A] {
  def buildParam(path: String): Stream[IO,A]
}

object SparkParamBuilderInstances {

  import cats.effect.IO
  import fs2.Stream
  import pureconfig.{ConfigReader, Derivation}

  import scala.reflect.ClassTag
  import java.nio.file.Path
  import java.nio.file.Paths

  import pureconfig.module.catseffect.loadConfigF

    def loadCnf[A](p: Path, ns: String)(
      implicit
      reader: Derivation[ConfigReader[A]],
      ct: ClassTag[A]
    ) = {
      import pureconfig.generic.auto._
      println(s"loading cnf: ${p} == ${ns}")
      Stream.eval(loadConfigF[IO, A](p, ns))
    }

  implicit  val paramBuilder: SparkParamBuilder[StandAloneParam] =
    new SparkParamBuilder[StandAloneParam] {
      override def buildParam(path: String): Stream[IO,StandAloneParam] = for {
        standAloneParam <- loadCnf[StandAloneParam](Paths.get(path), StandAloneParam.namespace)
      } yield standAloneParam
    }

}




