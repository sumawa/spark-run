package com.sa.sparkrun.conf

case class EnvConfig(activeEnv: String, homeDir: String, triggerInterval: Int) {

  def getExternalConfigPath: String =
    {println("GETTING EXTERNAL"); s"${this.homeDir}/${this.activeEnv}.conf".toLowerCase}

  def getApplicationConfPath: String = s"${this.homeDir}/application.conf".toLowerCase
}

object EnvConfig {
  val namespace: String = "env"
}
