package com.sa.sparkrun

import java.util.UUID

import cats.effect.{Blocker, ExitCode, IOApp}
import com.sa.sparkrun.conf.{DatabaseConfig, EnvConfig}
import com.sa.sparkrun.db.domain.job.Job
//import com.sa.sparkrun.submit.Launcher
import io.chrisdavenport.log4cats.StructuredLogger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import pureconfig.{ConfigReader, Derivation}
import com.sa.sparkrun.db.{DoobieJobRepository, PooledTransactor}
import com.sa.sparkrun.service.JobServiceImpl

import scala.reflect.ClassTag

object SparkMain extends IOApp {

  import cats.effect.IO

  private def logE(ioa: IO[_])(logger: StructuredLogger[IO]): IO[Unit] =
    ioa.attempt.flatMap(et => et.fold(ex => logger.error(ex)(ex.getMessage), _ => IO.unit))

  import java.nio.file.Path

  import fs2.Stream
  import pureconfig.module.catseffect.loadConfigF
  import pureconfig.generic.auto._

  private def loadCnf[A](p: Path, ns: String)(
    implicit
    reader: Derivation[ConfigReader[A]],
    ct: ClassTag[A]
  ): Stream[IO,A] =
    Stream.eval(loadConfigF[IO, A](p, ns))

  import com.sa.sparkrun.track.StandAloneMonitor

  import cats.implicits._

  import cats.effect.{IO,Timer,ContextShift}

  import scala.concurrent.duration._
  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val T: Timer[IO] = IO.timer(global)

//  import com.sa.sparkrun.params.SparkParamBuilderInstances._
  import com.sa.sparkrun.submit.SparkSubmitRunnerInstances._

//  private def init(blocker: Blocker) = for {
//    logger <- Stream.eval(Slf4jLogger.create[IO])
//    envConfig <- Stream.eval(loadConfigF[IO, EnvConfig](EnvConfig.namespace))
//    sparkRunner <- Stream.eval( sparkSubmitRunner.sparkRunner(java.nio.file.Paths.get(envConfig.getExternalConfigPath)))
//    monitor = new StandAloneMonitor[IO](blocker)
//    sparkCreateUrl <- Stream.eval(IO.pure("http://127.0.0.1:6066/v1/submissions/create"))
//    sparkRunIO = logE(IO(sparkRunner.run(sparkCreateUrl,envConfig.getExternalConfigPath)))(logger)
//    sparkMonitor = logE(IO(monitor.run()))(logger)
//    stream         <- Stream.awakeEvery[IO](10 seconds) >> Stream.eval((sparkRunIO,sparkMonitor).tupled.void)
//  } yield stream

  import java.nio.file.Paths

  private def init() = for {
    logger <- Stream.eval(Slf4jLogger.create[IO])
    envConfig <- Stream.eval(loadConfigF[IO, EnvConfig](EnvConfig.namespace))
    externalConfigPath = Paths.get(envConfig.getExternalConfigPath)
    sparkConfPath = Paths.get(envConfig.getExternalConfigPath)
    databaseConf <- loadCnf[DatabaseConfig](externalConfigPath, DatabaseConfig.namespace)
    _ <- Stream.eval(IO(println(s"got databaseConf: ${databaseConf}")))
    xa <- Stream.eval(PooledTransactor[IO](databaseConf))
    _ <- Stream.eval(IO(println(s"got XA: ${xa}")))
    jobR           = new DoobieJobRepository[IO](xa)
    _ <- Stream.eval(IO(println(s"got DoobieJobRepository: ${jobR}")))
    jobS           = new JobServiceImpl[IO](jobR)
    _ <- Stream.eval(IO(println(s"got JobServiceImpl: ${jobS}")))
    insertJ <- Stream.eval(
      IO(println(s"jobS.insert ${jobS.insert(Job(0,UUID.randomUUID().toString,"HEHE")).unsafeRunSync()}"))
    )
//    _ <- Stream.eval(IO(insertJ))
    jQ <-   Stream.eval(IO(println(s"jobS.allQueued ${jobS.allQueued.unsafeRunSync()}")))
    _ <- Stream.eval(IO(jQ))
      sparkRunner <- Stream.eval( IO (sparkSubmitRunner.sparkRunner(sparkConfPath)))
//    monitor = new StandAloneMonitor[IO](blocker)
    sparkCreateUrl <- Stream.eval(IO.pure("http://127.0.0.1:6066/v1/submissions/create"))
    sparkRunIO = logE(IO(sparkRunner.run(sparkCreateUrl,envConfig.getExternalConfigPath)))(logger)
//    sparkMonitor = logE(IO(monitor.run()))(logger)
//    stream         <- Stream.awakeEvery[IO](10 seconds) >> Stream.eval((sparkRunIO,sparkMonitor).tupled.void)
    stream         <- Stream.awakeEvery[IO](10 seconds) >> Stream.eval((sparkRunIO,IO.unit).tupled.void)
  } yield stream

//  override def run(args: List[String]): IO[ExitCode] =
//    Blocker[IO]
//      .use(init(_).compile.drain)
//      .handleError(println)
//      .as(ExitCode.Error)

  override def run(args: List[String]): IO[ExitCode] =
      init().compile.drain
        .handleError(println)
        .as(ExitCode.Error)

//  def runConcc(args:List[String]): IO[ExitCode] =
//    for{
////      _      <- if(args.length < 2)
////        IO.raiseError(new IllegalArgumentException("Need origin and destination files"))
////      else IO.unit
////      orig = new File(args.head)
////      dest = new File(args.tail.head)
////      count <- copyWConc[IO](orig,dest)
//      _ <- init()
//      _     <- IO(println(s"$count bytes copied from ${orig.getPath} to ${dest.getPath}"))
//
//    } yield ExitCode.Success

  //  private def init(blocker: Blocker) = for {
  //      logger <- Stream.eval(Slf4jLogger.create[IO])
  //      sparkParam <- paramBuilder.buildParam()
  //      monitor = new StandAloneMonitor[IO](sparkParam, blocker)
  //      launcher = new Launcher[IO](logger, blocker)
  //
  //      sparkMonitor = logE(IO(monitor.run()))(logger)
  //      sparkLauncher = logE(IO(launcher.run(sparkParam).unsafeRunSync()))(logger)
  //
  //      stream         <- Stream.awakeEvery[IO](10 seconds) >> Stream.eval((sparkLauncher,sparkMonitor).tupled.void)
  //  } yield stream

  //  private def init(blocker: Blocker) =
  //    for {
  ////      envConfig <- Stream.eval(loadConfigF[IO, EnvConfig](EnvConfig.namespace))
  ////      externalPath = Paths.get(envConfig.getExternalConfigPath)
  //      logger <- Stream.eval(Slf4jLogger.create[IO])
  //      submitterConf <- loadCnf[SubmitterConf](externalPath, SubmitterConf.namespace)
  //      daemonConf <- loadCnf[DaemonConf](externalPath, DaemonConf.namespace)
  //      databaseConf <- loadCnf[DatabaseConfig](externalPath, DatabaseConfig.namespace)
  //      yarnConf <- Stream.eval(loadConfigF[IO, YarnConfig](externalPath, YarnConfig.namespace))
  //      xa <- Stream.eval(PooledTransactor[IO](databaseConf))
  //      jobR = new DoobieJobInstanceRepository[IO](xa)
  //      jobS = new JobInstanceServiceImpl[IO](jobR)
  //      timeout = submitterConf.connectionTimeout.seconds
  //      clientFactoryL <- Stream.eval(Slf4jLogger.fromClass[IO](classOf[yarn.ClientFactory[IO]]))
  //      _ <- Stream.eval(logger.info(s"client config: ${yarnConf.clientConfig.toString}"))
  //      factory = new yarn.ClientFactory[IO](clientFactoryL, yarnConf.clientConfig, timeout, blocker)
  //      client <- Stream.eval(factory.makeAndConnect)
  //      submitter = YarnSubmitter.make[IO](client, submitterConf, hadoopConf, logger)
  //      appFactory = new ApplicationFactory[IO](logger, submitterConf, daemonConf)
  //      monitor = new YarnMonitor[IO](client, blocker)
  //      launcher = new Launcher[IO](submitter, appFactory, jobS, logger, blocker)
  //      resolver = new Resolver[IO](jobS, monitor, logger)
  //      work = (logE(launcher.run)(logger), logE(resolver.run)(logger))
  //      interval = daemonConf.periodicity.seconds
  //      stream <- Stream.awakeEvery[IO](interval) >> Stream.eval(work.tupled.void)
  //    } yield stream

}
