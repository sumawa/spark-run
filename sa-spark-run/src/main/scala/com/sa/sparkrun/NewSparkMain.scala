package com.sa.sparkrun

import java.util.UUID

import cats.effect.{Blocker, ExitCode, IOApp}
import com.sa.sparkrun.conf.{DatabaseConfig, EnvConfig}
import com.sa.sparkrun.db.domain.job.Job
import com.sa.sparkrun.params.SparkParamBuilderInstances
import com.sa.sparkrun.submit.Launcher
import com.sa.sparkrun.db.{DoobieJobRepository, PooledTransactor}
import com.sa.sparkrun.service.JobServiceImpl
import io.chrisdavenport.log4cats.StructuredLogger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import pureconfig.{ConfigReader, Derivation}

import scala.reflect.ClassTag

object NewSparkMain extends IOApp {

  import cats.effect.IO

  private def logE(ioa: IO[_])(logger: StructuredLogger[IO]): IO[Unit] =
    ioa.attempt.flatMap(et => et.fold(ex => logger.error(ex)(ex.getMessage), _ => IO.unit))

  import java.nio.file.Path

  import fs2.Stream
  import pureconfig.module.catseffect.loadConfigF

  import pureconfig.generic.auto._
  private def loadCnf[A](p: Path, ns: String)(
    implicit
    reader: Derivation[ConfigReader[A]],
    ct: ClassTag[A]
  ): Stream[IO,A] =
    Stream.eval(loadConfigF[IO, A](p, ns))

  import cats.effect.{IO, Timer}
  import cats.implicits._

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.duration._
  implicit val T: Timer[IO] = IO.timer(global)

  import com.sa.sparkrun.params.SparkParamBuilderInstances._

  import java.nio.file.Paths

  private def init(blocker: Blocker) = for {
    logger <- Stream.eval(Slf4jLogger.create[IO])
    envConfig <- Stream.eval(loadConfigF[IO, EnvConfig](EnvConfig.namespace))
    externalConfigPath = Paths.get(envConfig.getExternalConfigPath)
    sparkConfPath = Paths.get(envConfig.getExternalConfigPath)
    sparkParam <- paramBuilder.buildParam(envConfig.getExternalConfigPath)
    databaseConf <- loadCnf[DatabaseConfig](externalConfigPath, DatabaseConfig.namespace)
    _ <- Stream.eval(IO(println(s"got databaseConf: ${databaseConf}")))
    xa <- Stream.eval(PooledTransactor[IO](databaseConf))
    _ <- Stream.eval(IO(println(s"GOT ### XA: ${xa}")))
    jobR           = new DoobieJobRepository[IO](xa)
    _ <- Stream.eval(IO(println(s"GOT ### DoobieJobRepository: ${jobR}")))
    sparkCreateUrl <- Stream.eval(IO.pure("http://127.0.0.1:6066/v1/submissions/create"))
    launcher = new Launcher[IO](sparkCreateUrl, sparkParam, logger,blocker)
    sparkRunIO = logE(IO(launcher.run(jobR)))(logger)
    stream         <- Stream.awakeEvery[IO](10 seconds) >> Stream.eval((sparkRunIO,IO.unit).tupled.void)
  } yield stream

//  override def run(args: List[String]): IO[ExitCode] =
//      init().compile.drain
//        .handleError(e => println(s"BOOTSTRAP SPARK RUN ERROR OCCURED: \n\t ${e}"))
//        .as(ExitCode.Error)

  override def run(args: List[String]): IO[ExitCode] =
    Blocker[IO].use(init(_).compile.drain)
      .handleError(e => println(s"BOOTSTRAP SPARK RUN ERROR OCCURED: \n\t ${e}"))
      .as(ExitCode.Error)

  //  private def init(blocker: Blocker) =
  //    for {
  ////      envConfig <- Stream.eval(loadConfigF[IO, EnvConfig](EnvConfig.namespace))
  ////      externalPath = Paths.get(envConfig.getExternalConfigPath)
  //      logger <- Stream.eval(Slf4jLogger.create[IO])
  //      submitterConf <- loadCnf[SubmitterConf](externalPath, SubmitterConf.namespace)
  //      daemonConf <- loadCnf[DaemonConf](externalPath, DaemonConf.namespace)
  //      databaseConf <- loadCnf[DatabaseConfig](externalPath, DatabaseConfig.namespace)
  //      yarnConf <- Stream.eval(loadConfigF[IO, YarnConfig](externalPath, YarnConfig.namespace))
  //      xa <- Stream.eval(PooledTransactor[IO](databaseConf))
  //      jobR = new DoobieJobInstanceRepository[IO](xa)
  //      jobS = new JobInstanceServiceImpl[IO](jobR)
  //      timeout = submitterConf.connectionTimeout.seconds
  //      clientFactoryL <- Stream.eval(Slf4jLogger.fromClass[IO](classOf[yarn.ClientFactory[IO]]))
  //      _ <- Stream.eval(logger.info(s"client config: ${yarnConf.clientConfig.toString}"))
  //      factory = new yarn.ClientFactory[IO](clientFactoryL, yarnConf.clientConfig, timeout, blocker)
  //      client <- Stream.eval(factory.makeAndConnect)
  //      submitter = YarnSubmitter.make[IO](client, submitterConf, hadoopConf, logger)
  //      appFactory = new ApplicationFactory[IO](logger, submitterConf, daemonConf)
  //      monitor = new YarnMonitor[IO](client, blocker)
  //      launcher = new Launcher[IO](submitter, appFactory, jobS, logger, blocker)
  //      resolver = new Resolver[IO](jobS, monitor, logger)
  //      work = (logE(launcher.run)(logger), logE(resolver.run)(logger))
  //      interval = daemonConf.periodicity.seconds
  //      stream <- Stream.awakeEvery[IO](interval) >> Stream.eval(work.tupled.void)
  //    } yield stream

}
