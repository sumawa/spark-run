assemblyMergeStrategy in assembly := {
    case PathList("org","apache", "commons", xs @ _*) => MergeStrategy.last
    case PathList("OSGI-INF", "bundle.info") => MergeStrategy.first
    case x => {
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    }
}
