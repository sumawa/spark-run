package com.sa.common.fp

import scala.annotation.tailrec
import scala.util.control.TailCalls._


object MonadExm{

	def parseInt(str: String): Option[Int] = 
		scala.util.Try(str.toInt).toOption

	def divide(a: Int, b: Int): Option[Int] =
		if (b == 0) None else Some(a/b)

	def stringDivideBy(str1: String, str2: String) = 
		parseInt(str1).flatMap{ aNum =>
			parseInt(str2).flatMap { bNum => 
				divide(aNum,bNum)
			}
		}

	// Every monad is also a functor, so we can rely on both 
	// flatMap and map to sequence computations, we an use for comprehensions
	def sdb1(str1: String, str2: String) =
		for {
				a <- parseInt(str1)
				b <- parseInt(str2)
			} yield (divide(a,b))

	def sdb2(str1: String, str2: String) =
		for {
				a <- parseInt(str1)
				b <- parseInt(str2)
				x <- Option((1 to 3).toList)
				res <- divide(a,b)
				// x <- Option((1 to 3).toList)
				
			} yield (res,x.size)


	/*
		for better understanding of for comprehension and flatMapping compare this
		scala> MonadExm.listExm(List(1,2,3),List(4,5,6))
		res12: List[(Int, Int)] = List((1,4), (1,5), (1,6), (2,4), (2,5), (2,6), (3,4), (3,5), (3,6))

		scala> List(1,2,3).flatMap{ l1 => List(4,5,6).flatMap{ l2 => Option((l1,l2)) }}.toList
		res13: List[(Int, Int)] = List((1,4), (1,5), (1,6), (2,4), (2,5), (2,6), (3,4), (3,5), (3,6))

		So this is equivalent to:
			l1.flatMap{ a => l2.flatMap { b => Option((a,b))} }
	*/
	def listExm1(l1: List[Int], l2: List[Int]) = 
		for {
			x <- l1
			y <- l2
		} yield (x,y)

	def listExm2(l1: List[Int], l2: List[Int]) = 
		l1.flatMap{ a => l2.flatMap { b => Option((a,b))} }

	// BOTH listExm1 and listExm2 are equivalent

	def listSdbExm(l1: List[Int], str1: String, str2: String) = 
		for {
			x <- l1
			a <- parseInt(str1)
			b <- parseInt(str2)
			res <- divide(a,b)			
		} yield (x,a,b,res)

	def listSdbExm2(l1: List[Int], str1: String, str2: String) = 
		for {
			x <- l1
			a <- parseInt(str1)
			b <- parseInt(str2)
			res <- divide(a,b)			
		} yield (x,res)

	def listSdbExm3(l1: List[Int], l2: List[Int], str1: String, str2: String) = 
		for {
			x <- l1
			y <- l2
			a <- parseInt(str1)
			b <- parseInt(str2)
			res <- divide(a,b)			
		} yield (x,y,res)

	def listSdbExm4(l1: List[Int], l2: List[Int], str1: String, str2: String) = 
		for {
			a <- parseInt(str1)
			b <- parseInt(str2)

			x <- Option(l1)
			y <- Option(l2)
			res <- divide(a,b)			
		} yield (x,y,res)

	import cats.Monad					// for Monad
	import cats.instances.list._		// for List Monad
	import cats.instances.option._		// for Option Monad
	import cats.syntax.flatMap._		// for flatMap
	import scala.language.higherKinds._	// 
	import cats.syntax.functor._		// for map
	

	def sumSquare[F[_]: Monad](a : F[Int], b: F[Int]): F[Int] = 
		a.flatMap(x => b.map (y => x*x + y*y) )

	def sumSquareM[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
		for {
			x <- a
			y <- b
		} yield (x*x + y*y)

	/*
		package cats
		type Id[A] = A
		// Id is a type alias, that turns an atomic type into a single-parameter type 
		// constructor
	*/	

	sealed trait LoginError extends Product with Serializable
	type Result[A] = Either[Throwable,A]

	final case class UserNotFound(username: String) extends LoginError

	final case class PasswordIncorrect(username: String) extends LoginError

	case object UnexpectedError extends LoginError

	case class User(username: String, password: String)

	type LoginResult = Either[LoginError, User]

	def handleError(error: LoginError): Unit =
		error match {
			case UserNotFound(u) => println(s"User ${u} not found")
			case PasswordIncorrect(u) => println(s"Password incorrect for user: ${u}")
			case UnexpectedError => println(s"Unexpected error occurred")
		}

	// Trampolining using Eval defer
	def factorial(n: BigInt): BigInt = 
		if (n == 1) n else n * factorial(n-1) 


	import cats.Eval
	def factorialWEvalDefer(n: BigInt): Eval[BigInt] = 
		if (n == 1)
			Eval.now(n)
		else {
			Eval.defer(factorialWEvalDefer(n-1).map(_ * n))
		}
}

