package com.sa.common.fp.io

object IOExm extends App{
  import cats.effect.IO

  /*
    Value of type IO[A] is a computation, which, when evaluated, can perform effects before returning a value fo type A.
    IO values are pure, immutable values and thus preserves referential transparency.
    An IO is a data structure that represents just a description of a side effect-ful computation.

    IO can describe a synchronous or asynchronous computation that:
      - on evaluation yield exactly one result
      - can end in either success or failure and in case of failure flatMap chains get short-circuited (IO implementing the algebra of MonadError)
      - can be cancelled (user has to provide cancellation logic)

    Effects described via IO abstraction are not evaluated until the end (when one of the unsafe methods are used).
    Effect-ful results are not memoized (memory overhead is minimal and no leaks)
    A single effect may be run multiple times in a referentially transparent manner.
   */

  val ioExm = {
    val ioa = IO { println(s"hey! ${Math.random()}") }

    val program: IO[Unit] =
      for {
        _ <- ioa
        _ <- ioa
      } yield ()

    println(s"Calling unsafeRunSync")
    program.unsafeRunSync()
    //=> hey!
    //=> hey!
    ()

    def fib(n: Int, a: Long = 0, b: Long = 1): IO[Long] =
      IO(a + b).flatMap { b2 =>
        if (n > 0) fib(n - 1, b, b2)
        else IO.pure(b2)
      }

    println(s"fib : ${fib(50).unsafeRunSync()}")

    val lzyTest = IO.pure(25).flatMap(n => IO(println(s"Number is: $n")))

    lzyTest.unsafeRunSync()

    def putStrLn(value: String) = IO(println(value))
    val readLn = IO(scala.io.StdIn.readLine())

    val lzyTest1: IO[Unit] = for {
      _ <- putStrLn("What is your name?")
      n <- readLn
      _ <- putStrLn(s"Welcome! $n")
    } yield ()

    println(s"Running unsafeRunSync() ")
    lzyTest1.unsafeRunSync()
  }

  val asyncEffect = {

  }
}
