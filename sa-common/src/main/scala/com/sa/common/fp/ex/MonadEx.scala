package com.sa.common.fp.ex

object MonadEx extends App{
  /*
    Monad is a mechanism for sequencing computations.

    Functors allow us to sequence computations ignoring some complications.
    Functors allow this complication to occur once at the beginning of the sequence.
    They don't account further complications at each step in the sequence.

    Monad's flatMap allows us to specify what happens next, taking into account the immediate complication.
    Functor Laws
   */

  val failFast = {
    def parseInt(str: String): Option[Int] =
      scala.util.Try(str.toInt).toOption

    def divide(a: Int, b: Int): Option[Int] =
      if (b == 0) None else Some(a/b)

    // sequencing via flatMap allows fail-fast error handling behaviour
    // where a None at any step results in a None overall
    def stringDivideBy(aStr: String, bStr: String): Option[Int] =
      parseInt(aStr).flatMap { aNum =>
        parseInt(bStr).flatMap { bNum =>
          divide(aNum,bNum)
        }
      }

    //  println(stringDivideBy("6","2"))
    //  println(stringDivideBy("6","0"))
    //  println(stringDivideBy("6","foo"))
    //  println(stringDivideBy("bar","2"))

    def stringDivideByAlt(aStr: String, bStr: String): Option[Int] =
      for {
        aNum <- parseInt(aStr)
        bNum <- parseInt(bStr)
        res <- divide(aNum,bNum)
      } yield res

    //  println(stringDivideByAlt("6","2"))
    //  println(stringDivideByAlt("6","0"))
    //  println(stringDivideByAlt("6","foo"))
    //  println(stringDivideByAlt("bar","2"))
  }

  val monadLaws = {
    /*
      Monad Laws:
      Left Identity: calling pure and transforming the result with func is the same as calling func
      pure(a).flatMap(func) == func(a)

      Right Identity: passing pure to flatMap is the same as doing nothing
      a.flatMap(pure) == a

      Associativity: flatMapping over two functions f and g is the same as flatMapping over f and then flatMapping over g
      a.flatMap(f).flatMap(g) == a.flatMap( x => f(x).flatMap(g) )
     */
    import cats.Monad
    //  import cats.instances.list._
    import cats.instances.option._
    import cats.syntax.monad._

    def func1 (x: Int) = Option(x * 10)
    def func2 (x: Int) = Option(x + 5)

    // Left Identity Law
    // calling pure and transforming the result with func is the same as calling func
    // pure(a).flatMap(func) == func(a)
    def myPure(x: Int) = Monad[Option].pure(x)
    val isLeftIdentity = myPure(10).flatMap(func1(_)) == func1(10)
    println(s"isLeftIdentity: ${isLeftIdentity}")

    // Right Identity Law
    // passing pure to flatMap is the same as doing nothing
    // a.flatMap(pure) == a
    val isRightIdentity = Option(10).flatMap(myPure(_)) == Option(10)
    println(s"isRightIdentity: ${isRightIdentity}")

    // Associativity Law
    // a.flatMap(f).flatMap(g) == a.flatMap( x => f(x).flatMap(g) )
    // flatMapping over two functions f and g is the same as flatMapping over f and then flatMapping over g
    val isAssociative = Option(10).flatMap(a => func1(a)).flatMap(b => func2(b)) == Option(10).flatMap(a => func1(a).flatMap(b => func2(b)))
    println(s"isAssociative: ${isAssociative}")
  }

  val ex1 = {
      trait NewMonad[F[_]]{
        def pure[A](a: A): F[A]
        def flatMap[A,B](value: F[A])(func: A => F[B]): F[B]
        def map[A,B](value: F[A])(func: A => F[B]) = flatMap(value)( v => pure(func(v)))
      }
  }

  val futureMonad = {
    // Future Monad
    import cats.Monad
    import cats.instances.future._
    import scala.concurrent.Future
    import scala.concurrent.ExecutionContext.Implicits.global

    val fm = Monad[Future]
    val fut = fm.flatMap(fm.pure(5))(x => fm.pure(x + 3))

    import scala.concurrent.Await
    import scala.concurrent.duration._

    println(s"Evaluating future monad 2")
    val futRes = Await.result(fut, 6 seconds)
    println(s"futRes: ${futRes}")

  }

  val monadSyntax = {
    import cats.syntax.applicative._
    import cats.instances.list._
    import cats.instances.option._
    import cats.Monad

    println(1.pure[Option])
    println(1.pure[List])

    import cats.syntax.flatMap._    // for flatMap
    import cats.syntax.functor._    // for map
    //  import scala.language.higherKinds

    def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
      for {
        x <- a
        y <- b
      } yield (x*x + y*y)

    println(s"sumSquare(): ${sumSquare(Option(3), Option(4))}")
    println(s"sumSquare(): ${sumSquare(List(1,2,3), List(4,5,6))}")
  }

  val idMonad = {
    /*
      Id allows us to use monadic methods using plain values.
      Its definition is
        type Id[A] = A

      Id is actually a type alias that turns an atomic type to a single-parameter type constructor.
     */
    import cats.Monad
    import cats.syntax.functor._          // for map
//    import cats.syntax.applicative._      // for pure
    import cats.syntax.flatMap._          // for flatMap

    def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
      for {
        x <- a
        y <- b
      } yield (x*x + y*y)

    import cats.Id
    println(s" PRINTING USING Id Monad: ${sumSquare(3: Id[Int],4: Id[Int])}")

    println(s"${"Sum": Id[String]} -- ${3: Id[Int]}")

    val a = Monad[Id].pure(3)
    val b = Monad[Id].flatMap(a)(_ * 3)

    import cats.syntax.functor._            // for map
    import cats.syntax.flatMap._            // for flatMap
    val res = for{
      x <- a
      y <- b
    } yield (x + y)
    println(s"res: ${res}")
  }

  val ex2Id = {
    import cats.Id
    import cats.Monad

    def pure[A](value: A): Id[A] = value.asInstanceOf[Id[A]]
    def map[A,B](initial: Id[A])(func: A => B): Id[B] = func(initial)
    def flatMap[A,B](initial: Id[A])(func: A => Id[B]): Id[B] = func(initial)

    println(pure(123))
    println(map(123)(_ * 2))
    println(flatMap(123)(_ * 2))
  }

  val eitherMonad = {
    val either1: Either[String, Int] = Right(5)
    val either2: Either[String, Int] = Right(4)

    val res1 = for{
      a <- either1
      b <- either2
    } yield a + b

    println(s"res1: ${res1}")

    import cats.syntax.either._
    val aa = 3.asRight[String]
    val bb = 4.asLeft[String]

    val res2 = for{
      a <- aa
      b <- bb
    } yield a + b

    println(s"res2: ${res2}")

    def countPositive1(nums: List[Int]) = nums.foldLeft(0.asRight[String]){ (acc, num) =>
      if (num > 0) acc.map(_ + 1) else Left(s"Found ${num} which is Negative. Stopping. ")
    }
    println(countPositive1(List(1,2,3,4,5,-7,9)))

    println(Either.fromTry(scala.util.Try("abc".toInt)))
    println(s"${"Error".asLeft[Int].getOrElse(0)}")
    println(s"${-1.asRight[String].ensure(s"Must be Non Negative ")(_ > 0)}")

    val res3 = for {
      a <- 1.asRight[String]
      b <- 0.asRight[String]
      c <- if ( b == 0 ) "DIV0".asLeft[Int]
        else (a / b).asRight[String]
    } yield  c * 100
    println(s"res3: ${res3}")

  }

  val evalMonad = {
    // Eval represents two models of evaluations - Eager / Lazy + extra characteristic called "Memoized"
    import cats.Eval
    val now = Eval.now(Math.random() + 1000)
    val later = Eval.later(Math.random() + 2000)
    val always = Eval.always(Math.random() + 3000)

//    println(now.value)
//    println(later.value)
//    println(always.value)

    val greeting = Eval.always {println("Step 1"); "Hello"}
      .map {str => println("Step 2"); s"${str} world."}

//    println(greeting.value)

    // Eval.now will memoize A below.
    val ans = for {
      a <- Eval.now{println("Calculating A"); 40}
      b <- Eval.always{println("Calculating B"); 2}
    } yield {
      println("Adding A + B")
      a + b
    }

//    println(ans)
//    println(s"First access: ${ans.value}")
//    println(s"Second access: ${ans.value}")

    def factorial(n: BigInt): BigInt = if (n == 1) n  else n * factorial(n-1)

//    println(factorial(10000))
    def factorialWEval(n: BigInt): Eval[BigInt] = if (n == 1) Eval.now(n)
//    else Eval.defer(n * factorialWEval(n-1))
    else Eval.defer(factorialWEval(n-1).map(_ * n))

//    println(factorialWEval(10000).value)

    // Unsafe foldRight
    def foldRightUnSafe[A,B](as: List[A], acc: B)(fn: (A,B) => B): B =
      as match {
        case head :: tail => fn(head, foldRightUnSafe(tail, acc)(fn))
        case Nil => acc
      }

//    import cats.Monoid
//    import cats.instances.bigInt._
//    val fru = foldRightUnSafe((1 to 10000).toList,BigInt(0))( (x,y) => x + y)
//    println(fru)

    def foldRightWEval[A,B](as: List[A], acc: Eval[B])(fn: (A, Eval[B]) => Eval[B]): Eval[B] =
      as match {
        case head :: tail =>
          Eval.defer(fn(head, foldRightWEval(tail, acc)(fn)))
        case Nil => acc
      }

    println(s"foldRightWEval: ")
    val frwe = foldRightWEval((1 to 10000).toList,Eval.now(BigInt(0)))( (x,y) => y.map(_ + x))
    println(frwe.value)

  }

  val writerMonad = {
    import cats.data.Writer
    import cats.instances.vector._

    println(Writer(Vector("ABC","DEF"), 1234))

    import cats.syntax.applicative._      // for pure
    type Logged[A] = Writer[Vector[String],A]

    println(123.pure[Logged])

    import cats.syntax.writer._           // for tell and writer
    println(Vector("msg1", "msg2", "msg3").tell)

    val a = Writer(Vector("msg1","msg2","msg3"), 123)
    val b = 123.writer(Vector("msg1","msg2","msg3"))

    println(s"${a} ==== ${b}")
    println(s"${a.value} === ${a.written} === ${a.run}")

    val w1 = for{
      a <- 10.pure[Logged]
      _ <- Vector("Just logged 10").tell
      b <- 32.writer(Vector("\n now writing 32"))
    } yield a + b
//    println(s"${w1} ======")
//    println(s"${w1.written}")
    println(s"${w1.run}")
    println(s"${w1.mapWritten(_.map(_.toUpperCase)).run}")

    val neww1 = w1.bimap (
      log => log.map(_.toUpperCase)
      , res => res * 100
    )

    println(s"${neww1}")

    import cats.Eval
    def slowly[A](body: => A) = try body finally Thread.sleep(100)

    def factorialWEval(n: BigInt): Eval[BigInt] = {
      val res = slowly(
        if (n == 1) Eval.now(n)
        else { Eval.defer(factorialWEval(n - 1).map(_ * n)) }
      )
      println(s"fact $n ${res.value}")
      res
    }

    def factorialWWriter(n: Int): Logged[Int] = {
        for {
          res <- if (n == 0) 1.pure[Logged]
          else {
            slowly(
              factorialWWriter(n-1).map(_ * n)
            )
          }
          _ <- Vector(s"fact $n ${res}").tell
        } yield res
    }
//    println(s"${factorialWWriter(3).run}")
//    println(factorialWEval(5))

    import scala.concurrent.{Future,Await}
    import scala.concurrent.duration._
    import scala.concurrent.ExecutionContext.Implicits.global

    val fut1 = Future(factorialWWriter(3).run)
    val fut2 = Future(factorialWWriter(5).run)

    val futSeq = Future.sequence(Vector(fut1,fut2))
    val futRes = Await.result(futSeq, 5 seconds)
    println(futRes)
  }

  val readerMonad = {
    import cats.data.Reader
    import cats.syntax.applicative._        // for pure
//    import cats.implicits._
    import cats.instances.boolean._

    case class Db(userNames: Map[Int, String], passwords: Map[String,String])
    type DbReader[A] = Reader[Db,A]

    def findUserName(userId: Int): DbReader[Option[String]] =
      Reader(db => db.userNames.get(userId))

    def checkPassword(userName: String, password: String): DbReader[Boolean] =
      Reader(db => db.passwords.get(userName).contains(password))

    def defaultDbrBool = false.pure[DbReader]
    def checkLogin(userId: Int, password: String): DbReader[Boolean] =
      for {
        userName <- findUserName(userId)
        isValid <- userName.map(checkPassword(_, password)).getOrElse(defaultDbrBool)
      } yield isValid

    val users = Map(1 -> "dade", 2 -> "kate", 3 -> "margo")
    val passwords = Map("dade" -> "yesdade", "kate" -> "yeskate", "margo" -> "yesmargo")

    val db = Db(users,passwords)
    println(checkLogin(1, "yesdade").run(db))
    println(s"IS DbReader[Boolean] or Reader[Db,A] a Kliesli Arrow?: ${checkLogin(4, "davinci").run}")
  }

  val stateMonad = {
    import cats.data.State
    val s = State[Int, String]{ state => (state, s"The state is ${state}")}
    println(s.run(0).value)

    type CalcState[A] = State[List[Int], A]

    def evalOperator(f: (Int, Int) => Int) = State[List[Int],Int] {
      case (x1 :: x2 :: xs ) => (xs, f(x1,x2))
      case _ => sys.error("Error !!!")
    }
    def evalOne(sym: String): CalcState[Int] =
      sym match {
        case "+" => evalOperator((x1: Int, x2: Int) => x1 + x2)
        case "-" => evalOperator((x1: Int, x2: Int) => x1 - x2)
        case "*" => evalOperator((x1: Int, x2: Int) => x1 * x2)
        case "/" => evalOperator((x1: Int, x2: Int) => x1 / x2)
        case num => pushStack(num.toInt)
      }

    def pushStack(x: Int) = State[List[Int],Int]{ stack =>
      (x :: stack, x)
    }

    println(s"${evalOne("42").run(Nil).value}")

    import cats.syntax.applicative._          // for pure
    def evalAll(input: List[String]): CalcState[Int] =
      input.foldLeft(0.pure[CalcState]){ (a,b) =>
        a.flatMap(_ => evalOne(b))
      }

    def evalInput(input: String) =
      evalAll(input.split(" ").toList).runA(Nil).value

    println(s"${evalInput("1 2 + 3 4")}")

    sealed trait Maybe[A]{ self =>
      def isEmpty: Boolean
      def get: A
      final def flatMap[B](f: A => Option[B]): Option[B] =
        if (isEmpty) None else f(this.get)
    }
    final case class Just[A](value: A) extends Maybe[A]{
      override def isEmpty = false

      override def get: A = get
    }
    final case object Blank extends Maybe[Nothing] {
      override def isEmpty = true

      override def get: Nothing =  throw new NoSuchElementException("Blank.get")
    }

    import cats.Monad

//    val maybeMonad = new Monad[Maybe]{
//      override def pure[A](x: A): Maybe[A] = Just(x)
//
//      override def flatMap[A, B](fa: Maybe[A])(fn: A => Maybe[B]): Maybe[B] =
//        fa this.flatMap fn
//
//      override def tailRecM[A, B](a: A)(f: A => Maybe[Either[A, B]]): Maybe[B] =
//        f(a) match {
//          case Blank => Blank
//          case Just(a) => tailRecM(a)(f)
//        }
//    }

    import cats.Functor

    sealed trait Tree[+A]
    final case object ET extends Tree[Nothing]
    final case class Node[A](l: Tree[A], value: A, r: Tree[A]) extends Tree[A]

    object Tree{
      def node[A](tree: Tree[A]): Tree[A] = tree
      def et: Tree[Nothing] = ET
//      def bfTraverse[A,B](tree: Tree[A]): Tree[B] =
//        tree match {
//          case ET => = List()
//        }
    }

//    implicit val treeFunctor: Functor[Tree] =
//      new Functor[Tree] {
//        override def map[A, B](fa: Tree[A])(f: A => B): Tree[B] = fa match {
//          //        case ET => fa
//          case Node(ET,v,ET) => Node[B](ET, f(v), ET)
//          case Node(l,v,r) => Node(map(l)(f),f(v), map(r)(f))
//        }
//      }

    implicit val treeMonad = new Monad[Tree] {
      override def pure[A](x: A): Tree[A] = Node(ET,x,ET)
      // TODO: Challenge write flatMap for treeMonad
      override def flatMap[A, B](fa: Tree[A])(f: A => Tree[B]): Tree[B] = ???
//        fa match {
//          case Node(ET,v,ET) => f(v)
//          case Node(l,v,r) => Node[B](flatMap(l)(f), f(v), flatMap(r)(f))
//        }
      override def tailRecM[A, B](a: A)(f: A => Tree[Either[A, B]]): Tree[B] = ???
    }
    val t2 = Node(ET,2,ET)
    val t3 = Node(ET,3,ET)
    val t1 = Node(t2, 1, t3)
    println(t1)

//    println(Tree.node(t1).map[Int, Int](x => x * 10))

    val monadErrorEx = {
      import cats.MonadError
      import cats.instances.either._    // for MonadError

      type ErrorOr[A] = Either[String,A]
      val monadError = MonadError[ErrorOr,String]
      val success = monadError.pure(42)
      println(s"${success}")
      val failure = monadError.raiseError(s"BadMonadErrorRaise")
      println(s"${failure}")
      val hefail = monadError.handleError(failure){
        case "BadMonadErrorRaise" => monadError.pure("It's ok")
        case other => monadError.raiseError(s"It's not ok")
      }

      import cats.syntax.either._
      val hesucc = monadError.ensure(success)("Number too low !!!")(_ > 1000)
      println(s"hefaiL ${hefail} ----- hesucc ${hesucc}")
    }
  }

}

