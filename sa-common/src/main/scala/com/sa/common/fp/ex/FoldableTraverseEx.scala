package com.sa.common.fp.ex

object FoldableTraverseEx extends App{

  val foldableTraverseEx = {
    import cats.Eval
    import cats.Foldable

    def bigData = (1 to 100000).toStream
//    println(bigData.foldRight(0L)(_ + _))

    import cats.instances.stream._        // for Foldable

    val eval: Eval[Long] =
      Foldable[Stream].foldRight(bigData, Eval.now(0L)){ (num,eval) => eval.map(_ + num)}

    println(eval.value)
  }

}
