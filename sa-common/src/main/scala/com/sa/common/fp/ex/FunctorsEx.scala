package com.sa.common.fp.ex

import scala.concurrent.Await

object FunctorsEx extends App{

  import cats.Functor
  /*
    Functor Laws

    Identity:
    f(a) => a
   */
  println(List(1,2,3).map(a => a))

  /*
    Composition:

    (f . g) F = F map (f) map (g) = F.map(g(f(_)))
   */
  def f(x: Int) = x + 1
  def g(x: Int) = x * 2
  println(List(1,2,3).map(f).map(g))
  println(List(1,2,3).map(a => g(f(a)) ))

  // F map (f) map (g) = F.map(g(f(_))) also symbolically expressed as (f.g) F
  println(s"List(1,2,3).map(a => g(f(a)) ) == List(1,2,3).map(f).map(g) IS ${List(1,2,3).map(a => g(f(a)) ) == List(1,2,3).map(f).map(g)}")

  /*
   * Kinds are type of the types.
   * Type and Type constructor should be understood.
   * Ex
   * List is a type constructor which takes one parameter
   * But
   * List[A] is a type itself produced using a type parameter
   */
  import cats.Functor
  import cats.instances.option._
  val f1 = (x: Int) => x + 1

  /* lift makes
    A => B
    become
    F[A] => F[B]
    Here is the example where Functor is Option
   */
  val optF = Functor[Option].lift(f1)

  println(s"${optF} and optF(Option(1)) : ${optF(Option(1))}")

//  import cats.instances.function._
//  import cats.instances.int._
  import cats.syntax.functor._
  def doMath[F[_]](start: F[Int])(implicit functor: Functor[F]): F[Int] =
    start.map(n => n + 1 * 2)

  import cats.instances.option._
  println(doMath(Option(20)))
  import cats.instances.list._
  println(doMath(List(1,2,3)))

  case class Box[A](value: A)

  implicit val boxFunctor: Functor[Box] =
    new Functor[Box] {
      override def map[A, B](fa: Box[A])(f: A => B): Box[B] = Box(f(fa.value))
    }

  val box = Box[Int](5)
  println(box.map(x => Math.pow(x,2)))

  sealed trait Tree[+A]
  final case object ET extends Tree[Nothing]
  final case class Node[A](l: Tree[A], value: A, r: Tree[A]) extends Tree[A]

  object Tree{
    def node[A](tree: Tree[A]): Tree[A] = tree
    def et: Tree[Nothing] = ET
  }


  implicit val treeFunctor: Functor[Tree] =
    new Functor[Tree] {
      override def map[A, B](fa: Tree[A])(f: A => B): Tree[B] = fa match {
//        case ET => fa
        case Node(ET,v,ET) => Node[B](ET, f(v), ET)
        case Node(l,v,r) => Node(map(l)(f),f(v), map(r)(f))
      }
    }

  val t2 = Node(ET,2,ET)
  val t3 = Node(ET,3,ET)
  val t1 = Node(t2, 1, t3)
  println(t1)

 println(Tree.node(t1).map(x => x * 10))
}

