/*
	Cats effect and IO and http4s
	https://typelevel.org/blog/2018/06/07/shared-state-in-fp.html
*/

package com.sa.common.fp

import scala.annotation.tailrec
import scala.util.control.TailCalls._


object StateExm{

	/*
		instances of State[S,A] represent functions of type S => (S,A)
		where 
			S is a type of the State
			A is the type of result

		Or, an instance of State is a function that does two things
			- transforms an input state to an output state
			- computes a result
	*/
	import cats.data.State
	val a = State[Int,String] ( state => (state, s"The state is $state"))

	type CalcState[A] = State[List[Int], A]

	def operator(func: (Int,Int) => Int): CalcState[Int] = 
		State[List[Int], Int] {
			case b :: a :: tail =>
				val ans = func(a,b)
				(ans :: tail, ans)
			case _ => sys.error("Fail!")
		}

	def operand(num: Int): CalcState[Int] = 
		State[List[Int],Int] { stack =>
			(num :: stack, num)
		}
	// evalOne("42").runA(Nil).value
	def evalOne(sym: String): CalcState[Int] = 
		sym match {
			case "+" => operator(_ + _)
			case "-" => operator(_ - _)
			case "*" => operator(_ * _)
			case "/" => operator(_ / _)
			case num => operand(num.toInt)
		}

	val program = for {
		_ <- evalOne("1")
		_ <- evalOne("2")
		ans <- evalOne("*")
	} yield ans

	// program.runA(Nil).value

	val bigZero = BigInt(0)
	val bigOne = BigInt(1)

	// type CDState[A] = State[(BigInt,BigInt,BigInt), A]

	def shiftTuple(n: Int, tup: (BigInt,BigInt,BigInt)): (BigInt,BigInt,BigInt) = 
		n match {
			case x if (x <= 1) => (bigZero,bigZero,bigZero)
			case x if (x == 2) => (bigOne,bigOne,bigZero)
			case _ => 
				val currVal = (n-1) * (tup._2 + tup._3)
				(currVal,currVal,tup._2)
		}

	def next(i: Int): State[(BigInt,BigInt,BigInt),Int] =
    	State[(BigInt,BigInt,BigInt),Int] { tup =>
     		val nextVal = shiftTuple(i, tup)
     		(nextVal,i)
     	}

	def cdWState(n: Int): (BigInt,BigInt,BigInt)  = 
    	(1 to n).toList.foldLeft((bigZero,bigZero,bigZero)){ (acc,i) => 
    		next(i).runS(acc).value }

    def monadicVersion(n: Int) = for {
    	inis <- next(1)
    } yield inis

}

