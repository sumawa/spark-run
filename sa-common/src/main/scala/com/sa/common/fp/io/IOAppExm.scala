package com.sa.common.fp.io

import cats.Applicative.ops.toAllApplicativeOps
import cats.effect.ExitCase.Canceled
import cats.effect.{ConcurrentEffect, ExitCode, IO, IOApp}

import scala.concurrent.duration.DurationInt

object IOAppExm extends IOApp{

  /*
    * the command line arguments get passed as a pure List instead of an Array
    * we use an ExitCode to specify success or an error code,
    * the implementation handling how that is returned and thus we no longer have to deal with a side-effectful Runtime.exit call
    * the Timer[IO] dependency is already provided by IOApp
    * so on top of the JVM there’s no longer a need for an implicit ExecutionContext to be in scope
   */
  // EXAMPLE 1

//  override def run(args: List[String]): IO[ExitCode] =
//    args.headOption match {
//      case Some(name) => IO(println(s"Hello $name")).as(ExitCode.Success)
//      case None => IO(System.err.println(s"Usage: MyApp name")).as(ExitCode(2))
//    }

  // EXAMPLE 2
//  override def run(args: List[String]): IO[ExitCode] = loop(0).guaranteeCase {
//    case Canceled => IO(println("Interrupted: releasing and exiting"))
//    case _ => IO(println("Normal Exit"))
//  }
//
//  def loop(n: Int): IO[ExitCode] =
//    IO.suspend{
//      if (n < 10) IO.sleep(1 second) *> IO(println(s"Tick: $n")) *> loop(n + 1)
//      else IO.pure(ExitCode.Success)
//    }

  // EXAMPLE 3
  import cats.data.EitherT
  type F[A] = EitherT[IO,Throwable,A]
  val F = implicitly[ConcurrentEffect[F]]

  override def run(args: List[String]): IO[ExitCode] = F.toIO {
    EitherT.right(IO(println("Hello from EitherT !!!"))).map(_ => ExitCode.Success)
  }
}
