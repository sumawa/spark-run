package com.sa.common.fp.ex

/*
  Cats defines transformers for a variety of Monads,
  each providing the extra information we need to compose that monad
  with other monads.

  Ex: EitherT composes Either with other monads, OptionT composes Option with other monads.

  Another example:
    Suppose we want to transform List[Option[A]]
    We use OptionT to compose List and Option, by using OptionT[List,A]
    We can alias OptionT[List,A] to ListOption[A]
 */
object MTLEx extends App{
  /*
    Suppose we want to transform List[Option[A]]
    We use OptionT to compose List and Option, by using OptionT[List,A]
    We can alias OptionT[List,A] to ListOption[A]
   */
  import cats.data.OptionT
  // to compose List[Option[A]] we will go inside out, with OptionT containing a List
  // and alias like below.
  // The outer monad as a parameter of the transformer version of inner monad "<InnerMonad>T"
  type ListOption[A] = OptionT[List,A]

  import cats.Monad                       // for Monad
  import cats.syntax.applicative._        // for pure
  import cats.instances.list._

  val result1: ListOption[Int] = OptionT(List(Option(10),Option(20)))
  val result2: ListOption[Int] = 32.pure[ListOption]
  println(s"result1: ${result1} ------ result2: ${result2}")

  // Transformer OptionT with its "map" and "flatMap" methods  allowed us to use both component monads
  // without having to recursively unpack and repack values at each stage of computation.
  val resT1 = result1.flatMap{ (x: Int) =>
    result2.map{ y: Int =>
      (x * y)
    }
  }
  println(s"resT1: ${resT1}")

  type ErrorOr[A] = Either[String, A]
  type ErrorOrOption[A] = OptionT[ErrorOr,A]

  import cats.instances.either._    // for Either Monad
  val a = 32.pure[ErrorOrOption]
  val b = 10.pure[ErrorOrOption]

  val resT2 = a.flatMap( x => b.map(y => x + y))
  println(s"a: ${a} --- b: ${b} --- resT2 ${resT2}")

  import cats.data.EitherT
  import cats.instances.option._
  import cats.syntax.applicative._


  val resT3 = 32.pure[EitherT[Option,String,?]]
  println(s"resT3: ${resT3} --- resT3.value: ${resT3.value}")

  val resT4 = OptionT[ErrorOr,Int](Right(Some(10)))
  println(s"resT4: ${resT4} --- resT4.value: ${resT4.value} --- ${resT4.map(_ / 2).getOrElse(-1)}")

  val futEitherTEx = {
    import scala.concurrent.{Future,Await}
    import cats.data.{EitherT,OptionT}

    type FutureEither[A] = EitherT[Future,String,A]
    type FutureEitherOption[A] = OptionT[FutureEither,A]

    import cats.instances.future._            // for Monad
    import scala.concurrent.ExecutionContext.Implicits.global
    import scala.concurrent.duration._

    val futureEitherOpt: FutureEitherOption[Int] =
      for{
        a <- 10.pure[FutureEitherOption]
        b <- 20.pure[FutureEitherOption]
      } yield {
        Thread.sleep(2)
        a + b
      }

    println(s"futureEitherOpt: ${futureEitherOpt.value; Thread.sleep(100); futureEitherOpt}")
    //  println(s"futureEitherOpt: ${futureEitherOpt.value}")
  }

  sealed abstract class HttpError
  final case class NotFound(item: String) extends HttpError
  final case class BadRequest(item: String) extends HttpError

  import cats.data.Writer

  type Logged[A] = Writer[List[String],A]

  def parseNumber(str: String): Logged[Option[Int]] =
    util.Try(str.toInt).toOption match {
      case Some(num) => Writer(List(s"Read ${str}"), Some(num))
      case None => Writer(List(s"Failed on ${str}"), None)
    }

  import cats.data.OptionT
  def addAll(a: String, b: String, c: String): Logged[Option[Int]] =
    (for {
      a <- OptionT(parseNumber(a))
      b <- OptionT(parseNumber(b))
      c <- OptionT(parseNumber(c))
    } yield ( a + b + c )).value

  println(s"${addAll("1","2","3")}")
  println(s"${addAll("1","abc","3")}")

  // power level exercise
  val powerLevelEx = {
    import scala.concurrent.{Await,Future}

    import scala.concurrent.ExecutionContext.Implicits.global
    import cats.instances.future._    // for Monad
    import scala.concurrent.duration._

    val powerLevels = Map("Jazz" -> 6, "BumbleBee" -> 8, "Hot Rod"-> 10)

    //  type FutureString = Future[String]
    type Response[A] = EitherT[Future,String,A]
    def getPowerLevel(autobot: String): Response[Int] =
      powerLevels.get(autobot) match {
        case Some(ab) => EitherT.right(Future{ab})
        case None => EitherT.left(Future{s" autobot: ${autobot} doesn't exist."})
      }

    val powerLevelRes = getPowerLevel("Jazz")
    Thread.sleep(100)
    println(s"${powerLevelRes}")

//    import cats.instances.boolean
    def canSpecialMove(ally1: String, ally2: String): Response[Boolean] =
      for{
        pl1 <- getPowerLevel(ally1)
        pl2 <- getPowerLevel(ally2)
      } yield ((pl1 + pl2) > 15)

    val specialMovePossible = canSpecialMove("Jazz","BumbleBee")
    Thread.sleep(100)
    println(s"${specialMovePossible}")

    def tacticalReport(ally1: String, ally2: String) = {
      val stack1 = canSpecialMove(ally1, ally2).value
      Await.result(stack1, 1.second) match {
        case Left(msg) => s"Comms error: $msg"
        case Right(true) => s"$ally1 and $ally2 are ready to roll out"
        case Right(false) => s"$ally1 and $ally2 need a recharge"
      }
    }

    println(s"${tacticalReport("Hot Rod","BumbleBee")}")
  }

}
