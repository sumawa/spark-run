package com.sa.common.fp

import scala.annotation.tailrec
import scala.util.control.TailCalls._


object SemigroupalApplicativeExm{

	/*
		Functors monads lets us sequence operations using map and flatMap.

		But the code below fails fast on teh first call to parseInt 
		and doesn't go any further.

		Sometimes we want to return all the errors to the user, 
		and not stop on the first error we encounter.
	*/
	def failFastExm() = {
		import cats.syntax.either._		// for catchOnly

		def parseInt(str: String): Either[String,Int] =
			Either.catchOnly[NumberFormatException](str.toInt)
				.leftMap(_ => s"Couldn't read $str")

		for{
			a <- parseInt("a")
			b <- parseInt("b")
			c <- parseInt("c")
		} yield ( a + b + c )

	}

	/*
		Monadic comprehensions only allows us to run them in sequence.
		The map and flatMap aren't quite capable of capturing what we want 
		because they make the assumption that each computation is dependent 
		on the previous one. 

		The above parseInt and Future.apply are independent of one another. 
		But map and flatMap cannot exploit this.

		Semigroupal and Applicative supports this pattern of independent execution.

		* Semigroupal: encompasses notion of composing pairs of contexts.
		  Cats provides a cats.syntax.apply module that makes use of Semigroupal 
		  and Functor to allow users to sequence functions with multiple arguments

			trait Semigroupal[F[_]] {
				def product[A,B](fa: F[A], fb: F[B]): F[(A,B)]
			}

			fa and fb are independent of each other, can be computed in either order.
			In contrast to flatMap, which imposes a strict order on its parameters.

		* Applicative: extends Semigroupal and Functor. 
		  It provides a way of applying functions to parameters within a context.
		  Applicative is the source of pure method.
	*/

	case class Cat(name: String, yearOfBirth: Int, favoriteFoods: List[String])

	/*
		val garfield = Cat("Garfield",1978,List("Lasagna","Chicken"))
		val heathcliff = Cat("Heathcliff",1988,List("Junk food"))

		semigroupalMonoidExm(garfield,heathcliff)
		// res0: com.sa.common.fp.SemigroupalApplicativeExm.Cat = 
				// Cat(GarfieldHeathcliff,3966,List(Lasagna, Chicken, Junk food))
	*/
	def semigroupalMonoidExm(cat1: Cat, cat2: Cat) = {
		import cats.Monoid
		import cats.instances.int._			// for Monoid
		import cats.instances.invariant._	// for Semigroupal
		import cats.instances.list._		// for Monoid
		import cats.instances.string._		// for Monoid
		import cats.syntax.apply._			// for imapN

		def tupleToCat: (String, Int, List[String]) => Cat = Cat.apply _
		def catToTuple: Cat => (String, Int, List[String]) 
			= cat => (cat.name, cat.yearOfBirth, cat.favoriteFoods)

		implicit val catMonoid: Monoid[Cat] = (
			Monoid[String]
			, Monoid[Int]
			, Monoid[List[String]]
		).imapN(tupleToCat)(catToTuple)

		import cats.syntax.semigroup._ 		// for |+|
		cat1 |+| cat2
	}
}

