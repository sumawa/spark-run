package com.sa.common.fp

import cats.{Eq, Show}

import scala.annotation.tailrec
import scala.util.control.TailCalls._

/*
	Type classes are a programming pattern originated in Haskell. 

	Type classes allows extend esisting libraries with new functionality
		- without using traditional inheritance
		- without altering original library source code.

	Type classes can be tied to 
		- ADT
		- Pattern Matching
		- Value classes
		- Type aliases

	Three important component to the type class pattern:
		- Type class itself
		- instances for particular types
		- interface methods that we expose to the users.
	
*/
object Types extends App{

/*	
	Type class:
		- Is an interface or API that represents some functionality we want to implement.
		- In Cats a type class is represented by a trait with at least one type parameter.
*/
	sealed trait Json
	final case class JsObject(get: Map[String, Json]) extends Json
	final case class JsString(get: String) extends Json
	final case class JsNumber(get: Double) extends Json
	final case object JsNull extends Json

	// JsonWriter is our type class here 
	// with Json and its subtype provide supporting code
	// In Cats a type class is represented by a trait with at least one type parameter.
	trait JsonWriter[A]{
		def write(value: A): Json
	}

/*	
	Type class instance:
	 - provide implementations for the types: our own types or the ones from libraries. 
	 - are defined by creating concrete implementations and tag them with implicit keyword.
*/
	final case class Person(name: String, email: String)

	// REMEMBER: Any definitions marked implicit in Scala must be placed inside and object
	// or a trait and not at the top level.

	object JsonWriterInstances{
		implicit val stringWriter: JsonWriter[String] = 
			new JsonWriter[String]{
				def write(value: String): Json = JsString(value)
			}

		implicit val personWriter: JsonWriter[Person] = 
			new JsonWriter[Person]{
				def write(value: Person): Json = JsObject(
						Map(
							"name" -> JsString(value.name)
							, "email" -> JsString(value.email)
							)
					)
			}
	}

	/*	
		Type class interfaces:
			- any functionality we exposed to the users.
			- are generic methods that accept instances of the type class as implicit parameters.

	*/

	/*
		Two common ways of specifying an interface: 
	*/

	/*		
		- Interface Objects (Simplest way):
				- place methods in a singleton object
	*/
	import JsonWriterInstances._


	object Json{
		// generic methods that accept instances of the type class as implicit parameters
		def toJson[A](value :A)(implicit w: JsonWriter[A]): Json = 
			w.write(value)
	}

	def runSingletonObj() = {
		val res1 = Json.toJson(Person("SA", "saemail@email.email"))
		println(s"res1: ${res1}")
	}
	/*
		Interface Syntax: Use extension methods to extend existing types with interface methods.
		Cats refers to thsi as "syntax" for the type class.

	*/
	object JsonSyntax{
		implicit class JsonWriterOps[A](value: A){
			def toJson(implicit w: JsonWriter[A]): Json = w.write(value)
		}
	}

	/* IMPLICIT SCOPE: 
		Use interface syntax by importing it alongside the instances for the types we need.
		Like here we have packaged our type class instances in an object JsonWriterInstances.

		Placing instances in companion object to the type class has special significance in Scala.
		Because it plays into something called IMPLICIT SCOPE.
	*/

	import JsonWriterInstances._
	import JsonSyntax._

	def runJsonSyntax() = {
		val res2 = Person("SA","saemail@email.email").toJson
		println(s"res2: ${res2}")		
	}

	/*
		We can use implicitly to summon any value from implicit scope, like this
	*/
	implicitly[JsonWriter[String]]
	// res6: com.sa.common.fp.Types.JsonWriter[String] = com.sa.common.fp.Types$JsonWriterInstances$$anon$1@2d906638

	/*
		We can package type class instances in roughly four ways:
			1. by placing them in an object such as JsonWriterInstances;
			2. by placing them in a trait;
			3. by palcing them in the companion object of the type class;
			4. by placing them in the companion of the parameter type;
	*/


	/*
		Recursive Implicit Resolution

		POWER of Type Classes and implicits lies in 
			- the compiler's ability to combine implict definitions when searching for candidate instances.
		
		Instance can be defined in two ways:
			- By defining concrete instances as implicit vals of the required type.
			- By defining implicit methods to construct instances from other type class instances.			

	*/

	// When we create a type class instance constructor using an implicit def
	// we must mark the parameters to the method as implicit parameters. 
	// Without this keyword the compiler won't be able to fill in the parameters during implicit resolution.
	// Implicit methods with non-implicit parameters for a Scala pattern 
	// called an implicit conversion.	
	implicit  def optionalWriter[A](implicit writer: JsonWriter[A]): JsonWriter[Option[A]] =
		new JsonWriter[Option[A]] {
			def write(option: Option[A]): Json =
				option match {
					case Some(aValue) => writer.write(aValue)
					case None 		  => JsNull
				}
		}	

	// implicit val personShow: Show[Person] = 
	// new Show[Person] {
	//     def show(person: Person): String = s"name: ${person.name} email: ${person.email}"
	// }

  import cats.syntax.show._
//  import cats.instances.string._

  final case class Cat(name: String, age: Int, color: String)

  object Cat{
    implicit val catShow: Show[Cat] = new Show[Cat] {
      override def show(t: Cat): String = s"${t.name} is ${t.age} years old and is of ${t.color} color "
    }

    import cats.instances.string._
    import cats.instances.int._
    import cats.syntax.eq._   // for === and =!=

    implicit val catEq: Eq[Cat] =
      Eq.instance[Cat] { (cat1, cat2) =>
        cat1.name === cat2.name && cat1.age === cat2.age && cat1.color === cat2.color
      }
  }



}

