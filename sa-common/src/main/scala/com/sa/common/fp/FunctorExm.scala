package com.sa.common.fp

import scala.annotation.tailrec
import scala.util.control.TailCalls._


object FunctorExm{

	import scala.concurrent.{Future,Await}
	import scala.concurrent.ExecutionContext.Implicits.global
	import scala.concurrent.duration._
	import scala.util.Random

	def futureTest = {

		val future: Future[String] = 
			Future(123).map(n => n+1)
				.map(n => n+2)
				.map(n => n+"!")

		Await.result(future,2.second)		
	}

	def futureTest2 = {
		val future1 = {
			val r = new Random(0L)

			val x = Future(r.nextInt)

			for {
				a <- x
				b <- x
			} yield (a,b)
		}

		val future2 = {
			val r = new Random(0L)

			for {
				a <- Future(r.nextInt)
				b <- Future(r.nextInt)
			} yield (a,b)
		}

		val res1 = Await.result(future1, 1.second)
		val res2 = Await.result(future2, 1.second)

		println(s"$res1 = {res1}")
		println(s"$res2 = {res2}")

	}


	def funcSequencing = {
		import cats.instances.function._
		import cats.syntax.functor._

		val f1: Int => Double = (x:Int) => x.toDouble
		// f1: Int => Double = $$Lambda$5591/679006108@699fcc1c

		val f2: Double  => Double = (x:Double) => x * 2
		// f2: Double => Double = $$Lambda$5594/417992633@2cc76095

		println( (f1 map f2) (5) )

	}
	def functorTest(lf: Int => Int, of: Int => Int) = {
		import scala.language.higherKinds
		import cats.Functor
		import cats.instances.list._
		import cats.instances.option._

		val list1 = List(1,2,3)
		val list2 = Functor[List].map(list1)(_ * 2)
		val lfv = Functor[List].map(list1)(lf)

		println(s"list2: ${list2} -- lfv: ${lfv}")

		val option1 = Option(123)
		val option2 = Functor[Option].map(option1)(_ * 2)
		val ofv = Functor[Option].map(option1)(of)
		println(s"option2: ${option2} -- ofv: ${ofv}")

		val func = (x: Int) => x + 1
		val oliftedFunc = Functor[Option].lift(func)
		val lliftedFunc = Functor[List].lift(func)

		println(s"oliftedFunc(Option(1)): ${oliftedFunc(Option(1))}")
		println(s"lliftedFunc(List(1,2,3)): ${lliftedFunc(List(1,2,3))}")
	}

	def functorSyn = {
		import cats.Functor
		import cats.instances.function._
		import cats.syntax.functor._

		def doMath[F[_]](start: F[Int]) (implicit functor: Functor[F]): F[Int] =
			start.map(n => n + 1 * 2)

		import cats.instances.option._
		import cats.instances.list._

		println(s"doMath(Option(20)) --- ${doMath(Option(20))}")
		println(s"doMath(List(1,2,3)) --- ${doMath(List(1,2,3))}")
	}

}

