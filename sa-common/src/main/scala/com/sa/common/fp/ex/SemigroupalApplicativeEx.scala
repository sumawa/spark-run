package com.sa.common.fp.ex

object SemigroupalApplicativeEx extends App{

  val semigroupalEx = {
    import cats.Semigroupal
    import cats.instances.option._

    println (Semigroupal[Option].product(Some(123),Some("abc")))
    println (Semigroupal[Option].product(Some(123),Some(456)))
    println (Semigroupal[Option].product(Some(123),None))

    import cats.instances.option._
    println (Semigroupal.tuple3(Option(123),Option("abc"),Option(3)))

    import cats.syntax.apply._

    println((Option(123),Option("abc")).tupled)
    case class Cat(name : String, age: Int, color: String)

    println (  (Option("Garfield"),Option(5),Option("Orange")).mapN(Cat.apply) )

    import cats.Monoid
    import cats.instances.int._       // for Monoid
    import cats.instances.string._    // for Monoid
    import cats.instances.invariant._ // for Semigroupal
    import cats.syntax.apply._        // for imapN
    import cats.instances.set._       // for Monoid

    case class NCat(name: String, age: Int, foods: Set[String])

    val tupleToCat: (String,Int,Set[String]) => NCat = NCat.apply _
    val catToTuple: NCat => (String,Int,Set[String]) = cat => (cat.name, cat.age, cat.foods)

    import cats.syntax.semigroup._
    implicit val ncatMonoid: Monoid[NCat] = (Monoid[String], Monoid[Int], Monoid[Set[String]]).imapN(tupleToCat)(catToTuple)
    val garfield = NCat("Garfield",5,Set("fish","chicken"))
    val heathcliff = NCat("HeahtCliff",7,Set("fish","milk"))

    println(garfield |+| heathcliff)

    import cats.instances.list._
    println(Semigroupal[List].product(List(1,2,3), List(3,4)))

    import cats.instances.either._
    type ErrorO[A] = Either[Vector[String],A]
    //    Semigroupal[ErrorO].product(Left("E 1"), Left("E 2"))

    import cats.syntax.flatMap._
    import cats.syntax.functor._

    import cats.Monad
    def product[M[_]: Monad,A,B](x: M[A], y: M[B]): M[(A,B)] =
      x.flatMap( a => y.map ( b => (a,b)))

  }

  val validatedEx = {
    import cats.Semigroupal
    import cats.data.Validated
    import cats.instances.vector._

    type AllErrorsOr[A] = Validated[Vector[String],A]

    val prodAllErrorOr = Semigroupal[AllErrorsOr].product(
      Validated.invalid(Vector("Error 1"))
      , Validated.invalid(Vector("Error 2"))
    )
    println(s"${prodAllErrorOr}")

    val v = Validated.Valid(123)
    val i = Validated.Invalid(Vector("Very bad"))
    def piv(v: Validated[Vector[String],Int], i:  Validated[Vector[String],Int]) =  println(s"${v} -- ${i}")

    piv(v,i)
    piv(Validated.valid[Vector[String],Int](123),  Validated.invalid[Vector[String]
      ,Int](Vector("Error V")))

    import cats.syntax.validated._
    piv(123.valid[Vector[String]], Vector("Very bad Error").invalid[Int])

    import cats.syntax.applicative._          // for pure
    import cats.syntax.applicativeError._     // for raiseError
//    import cats.instances.vector._
    println(123.pure[AllErrorsOr])
    println(Vector("Bad V Error").raiseError[AllErrorsOr,Int])

    println(Validated.catchOnly[NumberFormatException]("foo".toInt))
    println(Validated.catchNonFatal(sys.error("Badness")))
    println(Validated.fromTry(scala.util.Try("foo".toInt)))
    println(Validated.fromEither[String,Int](Left("Badness")))

    import cats.data.Validated

    type FormData = Map[String,String]
    type FailFast[A] = Either[Vector[String],A]
    type FailSlow[A] = Validated[Vector[String],A]

    def getValue(name: String)(data: FormData): FailFast[String] =
      data.get(name).toRight(Vector(s"$name field not specified"))

    val getName = getValue("name") _
    println(s"${getName}")

    println(s"${getName(Map("name" -> "Dade Murphy"))}")

    import cats.syntax.either._

    type NumFmtExn = NumberFormatException
    def parseInt(name : String)(data: String): FailFast[Int] =
      Either.catchOnly[NumFmtExn](data.toInt)
        .leftMap(_ => Vector(s"$name must be an integer"))

    println(parseInt("age")("11c"))

    def nonBlank(name: String)(data:String): FailFast[String] =
      Right(data).ensure(Vector(s"$name cannot be blank"))(_.nonEmpty)
    def nonNegative(name: String)(data:Int): FailFast[Int] =
      Right(data).ensure(Vector(s"$name cannot be negative"))(_ > 0)

    println(s"${nonBlank("name")("")}")
    println(s"${nonNegative("age")(-11)}")

    def readName(data:FormData): FailFast[String] =
      getValue(("name"))(data)

    def readAge(data: FormData): FailFast[Int] =
      getValue("age")(data)
        .flatMap(nonBlank("age"))
        .flatMap(parseInt("age"))
        .flatMap(nonNegative("age"))

    println(s"readAge: ${readAge(Map( "age" -> "11"))}")
    println(s"readAge: ${readAge(Map( "age" -> ""))}")
    println(s"readAge: ${readAge(Map( "age" -> "-1"))}")
  }

}
