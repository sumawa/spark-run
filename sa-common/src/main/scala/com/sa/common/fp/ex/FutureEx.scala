package com.sa.common.fp.ex

import scala.concurrent.Await

object FutureEx extends App{

    import scala.concurrent.{Future}
    import scala.concurrent.ExecutionContext.Implicits.global
    import scala.concurrent.duration._

    val f = Future(123).map(n => n+1)
      .map(n => n * 10)
      .map(n => n + "!!!")

    //    f.onComplete(s => println(s"COMPLETED : ${s}"))
    val res = Await.result(f, 2 second)
    println(s"res: ${res}")

//    import cats.instances.function._    // for Functor
//    import cats.syntax.functor._        // for map
//    val func = ( (x: Int) => x.toDouble ).map(x => x+1)
//      .map(x => x * 2)
//      .map(x => x + "!!!")
//   println(func(123))

  val r = for {
    a <- Future{ println("A Waiting"); Thread.sleep(3000); println("A COMPLETED"); 7}
    b <- Future { println("B Waiting"); Thread.sleep(3000); println("B COMPLETED"); 3}
  } yield (a + b)

  println(s"PRINTING r: ${r}")

  Thread.sleep(7000)
  println(s"PRINTING r AFTER SLEEP: ${r}")
}
