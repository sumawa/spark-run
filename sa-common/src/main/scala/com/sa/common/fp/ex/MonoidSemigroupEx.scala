package com.sa.common.fp.ex

import cats.{Monoid, Semigroup}

object MonoidSemigroupEx extends App{
  object BoolAnd {
    implicit val booleanAndMonoid: Monoid[Boolean] =
      new Monoid[Boolean] {
        override def empty: Boolean = true
        override def combine(x: Boolean, y: Boolean): Boolean = x && y
      }
  }

  object BoolOr {
    implicit val booleanOrMonoid: Monoid[Boolean] =
      new Monoid[Boolean] {
        override def empty: Boolean = false
        override def combine(x: Boolean, y: Boolean): Boolean = x || y
      }
  }

  object BoolXOR {
    // exclusiveOr (either is true)
    implicit val booleanXORMonoid: Monoid[Boolean] =
      new Monoid[Boolean] {
        override def empty: Boolean = false
        override def combine(x: Boolean, y: Boolean): Boolean =
          (x && !y) || (!x && y)
      }
  }

  object BoolNOR {
    implicit val booleanNORMonoid: Monoid[Boolean] =
      new Monoid[Boolean] {
        override def empty: Boolean = true
        override def combine(x: Boolean, y: Boolean): Boolean =
          (!x || y) && (x || !y)
      }
  }

  object SetUnionType {
    implicit def setUnionMonoid[A]: Monoid[Set[A]] =
      new Monoid[Set[A]] {
        override def empty: Set[A] = Set.empty[A]
        override def combine(x: Set[A], y: Set[A]): Set[A] =
          x union y
      }
  }
  object SetIntersectionType {
    implicit def setIntersectionMonoid[A]: Semigroup[Set[A]] =
      new Semigroup[Set[A]] {
        override def combine(x: Set[A], y: Set[A]): Set[A] = x intersect(y)
      }
  }

  object SetSymDiffType {
    implicit def setSymDiffMonoid[A]: Monoid[Set[A]] =
      new Monoid[Set[A]] {
        override def empty: Set[A] = Set.empty
        override def combine(x: Set[A], y: Set[A]): Set[A] =
          (x diff(y)) union( y diff(x))
      }
  }

  object SuperAdd{
    // older style of specifying implicit monoid in currying way
    // def add[A](items: List[A])(implicit monoid: Monoid[A])

    // This style of specifying A: Monoid in Type is known as context bound syntax
    // it is better than the above example
    def add[A: Monoid](items: List[A]): A =
      items.foldLeft(Monoid[A].empty)( (acc,i) => Monoid[A].combine(acc,i) )

    // Aother way of writing, BUT we will have to specify semigroup syntax import
    import cats.syntax.semigroup._
    def add2[A: Monoid](items: List[A]): A =
      items.foldLeft(Monoid[A].empty)( _ |+| _  )

  }
  override def main(args: Array[String]): Unit = {

    val set1 = Set(1,2)
    val set2 = Set(2,3,4)

    import SetSymDiffType._
    val res = Monoid[Set[Int]].combine(set1,set2)
    println(s"result of combine of ${set1} and ${set2} is: ${res}")

//    import SetIntersectionType._
//    val res2 = Semigroup[Set[Int]].combine(set1,set2)
//    println(s"result of semigroup combine of ${set1} and ${set2} is: ${res2}")  // result of semigroup combine of Set(1, 2) and Set(2, 3, 4) is: Set(2)

    import SuperAdd._
    import cats.instances.int._
    println(s"Monoidically add (combine) ints : ${add[Int](List(1,2,3,4))}" )
    import cats.instances.double._
    println(s"Monoidically add (combine) doubles : ${add[Double](List(1.5,2.3,3.6,4.9))}" )
    import cats.instances.string._
    println(s"Monoidically add (combine) string : ${add[String](List("a","b","c"))}" )
    import cats.instances.string._
    println(s"Monoidically add2 (combine) string : ${add2[String](List("a","b","c"))}" )
    import cats.instances.double._
    println(s"Monoidically add2 (combine) doubles : ${add2[Double](List(1.5,2.3,3.6,4.9))}" )

  }
}
