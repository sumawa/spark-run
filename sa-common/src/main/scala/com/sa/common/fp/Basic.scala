package com.sa.common.fp

import scala.annotation.tailrec
import scala.util.control.TailCalls._


object Basic{

	def even(i: Int): Boolean = i match {
		case 0 => true
		case _ => odd(i-1)
	}

	def odd(i: Int): Boolean = i match {
		case 0 => false
		case _ => even(i-1)
	}

	@tailrec
	def evenTR(i: Int): Boolean = i match {
		case 0 => true
		case 1 => false
		case _ => evenTR(i-2)
	}

	def oddTR(i: Int): Boolean = !(evenTR(i))

	sealed trait Computation[A]
	case class Continue[A](nextF: () => Computation[A]) extends Computation[A]
	case class Completed[A](result: A) extends Computation[A]

	def evenC(i: Int): Computation[Boolean] = i match {
		case 0 => Completed(true)
		case _ => Continue(() => oddC(i-1))
	}

	def oddC(i: Int): Computation[Boolean] = i match {
		case 0 => Completed(false)
		case _ => Continue(() => evenC(i-1))
	}

	@tailrec
	def run[A](computation: Computation[A]): A = computation match {
		case Completed(a) => a
		case Continue(nextF) => run(nextF())
	}

	// using scala TailRec util
	def evenTC(i: Int): TailRec[Boolean] = i match {
		case 0 => done(true)
		case _ => tailcall(oddTC(i-1))
	}

	def oddTC(i: Int): TailRec[Boolean] = i match {
		case 0 => done(false)
		case _ => tailcall(evenTC(i-1))
	}
}

