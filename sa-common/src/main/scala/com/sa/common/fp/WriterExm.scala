package com.sa.common.fp

import scala.annotation.tailrec
import scala.util.control.TailCalls._


object WriterExm{

	import cats.syntax.writer._
	import cats.instances.vector._
	import cats.syntax.applicative._
	import cats.data.Writer

	type Logged[A] = Writer[Vector[String],A]

	val writer1 = for {
	    a <- 10.pure[Logged]
	    _ <- Vector("a","b","c").tell
	    b <- 32.writer(Vector("x","y","z"))
	} yield a + b

}

