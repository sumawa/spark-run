package com.sa.common.ctexm


// MUST https://www.youtube.com/watch?v=LhGq4HlozV4
// https://softwaremill.com/free-monads/
// https://efekahraman.github.io/2019/03/an-example-of-free-monads-and-optimization
// https://www.slideshare.net/hermannhueck/composing-an-app-with-free-monads-using-cats

object FMInteractive extends App{

   // val names = Array("chris", "ed", "maurice")

	// val userBase = List(User("Travis", 28),
 //      User("Kelly", 33),
 //      User("Jennifer", 44),
 //      User("Dennis", 23))

 //    val twentySomethings = for { //( is not used when definitions are present
 //      user <- userBase //Generators
 //      age = user.age //Definitions
 //      if age >= 20 && age < 30 //Filters
 //    }
 //      yield user.name  // i.e. add this to a list

 //    //Compiler converts it to following
 //    //val twentySomethings = userBase.
 //    // withFilter(((user) => user.age.$greater$eq(20).
 //    // $amp$amp(user.age.$less(30)))).
 //    // map(((user) => user.name));


	import cats.data.EitherK
	import cats.free.Free
	import cats.{Id, InjectK, ~>}
	import scala.collection.mutable.ListBuffer

	/* Handles user interaction */
	sealed trait Interact[A]
	case class Ask(prompt: String) extends Interact[String]
	case class Tell(msg: String) extends Interact[Unit]

	/* Represents persistence operations */
	sealed trait DataOp[A]
	case class AddCat(a: String) extends DataOp[Unit]
	case class GetAllCats() extends DataOp[List[String]]

	type CatsApp[A] = EitherK[DataOp, Interact, A]

	class Interacts[F[_]](implicit I: InjectK[Interact, F]) {
	  def tell(msg: String): Free[F, Unit] = Free.inject[Interact, F](Tell(msg))
	  def ask(prompt: String): Free[F, String] = Free.inject[Interact, F](Ask(prompt))
	}

	object Interacts {
	  implicit def interacts[F[_]](implicit I: InjectK[Interact, F]): Interacts[F] = new Interacts[F]
	}

	class DataSource[F[_]](implicit I: InjectK[DataOp, F]) {
	  def addCat(a: String): Free[F, Unit] = Free.inject[DataOp, F](AddCat(a))
	  def getAllCats: Free[F, List[String]] = Free.inject[DataOp, F](GetAllCats())
	}

	object DataSource {
	  implicit def dataSource[F[_]](implicit I: InjectK[DataOp, F]): DataSource[F] = new DataSource[F]
	}

	def program(implicit I : Interacts[CatsApp], D : DataSource[CatsApp]): Free[CatsApp, Unit] = {

	  import I._, D._

	  for {
	    cat <- ask("What's the kitty's name?")
	    _ <- addCat(cat)
	    cats <- getAllCats
	    _ <- tell(cats.toString)
	  } yield ()
	}

	object ConsoleCatsInterpreter extends (Interact ~> Id) {
	  def apply[A](i: Interact[A]) = i match {
	    case Ask(prompt) =>
	      println(prompt)
	      readLine()
	    case Tell(msg) =>
	      println(msg)
	  }
	}

	object InMemoryDatasourceInterpreter extends (DataOp ~> Id) {

	  private[this] val memDataSet = new ListBuffer[String]

	  def apply[A](fa: DataOp[A]) = fa match {
	    case AddCat(a) => memDataSet.append(a); ()
	    case GetAllCats() => memDataSet.toList
	  }
	}

	def exec() = {
		val interpreter: CatsApp ~> Id = InMemoryDatasourceInterpreter or ConsoleCatsInterpreter

		import DataSource._, Interacts._
		val evaled: Unit = program.foldMap(interpreter)
	}

}
