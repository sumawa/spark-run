package com.sa.common.ctexm

import cats._
import cats.data._
import cats.implicits._

// MUST https://www.lihaoyi.com/post/TheDeathofHypeWhatsNextforScala.html

// MUST https://www.youtube.com/watch?v=LhGq4HlozV4
// https://softwaremill.com/free-monads/
// https://efekahraman.github.io/2019/03/an-example-of-free-monads-and-optimization


object FMArray extends App{

	sealed trait ArrayA[A]
	case class Put[T](idx: Int, value: T) extends ArrayA[Unit]
	case class Get[T](idx: Int) extends ArrayA[Option[T]]


	import cats.free.Free

	type ArrayStore[A] = Free[ArrayA, A]

	import cats.free.Free.liftF

	// Put returns nothing (i.e. Unit).
	def put[T](idx: Int, value: T): ArrayStore[Unit] =
	  liftF[ArrayA, Unit](Put[T](idx, value))

	// Get returns a T value.
	def get[T](idx: Int): ArrayStore[Option[T]] =
	  liftF[ArrayA, Option[T]](Get[T](idx))

	def program(i:Int): ArrayStore[Option[Int]] =
		for {
				_ <- put[Int](1, 0)
				_ <- put[Int](2, 0)
				// if (arr(3) == -1) put[Int](3,11) 
				n <- get[Int](i) 
			} yield n



	import cats.arrow.FunctionK
	import cats.{Id, ~>}
	import scala.collection.mutable

	// the program will crash if a key is not found,
	// or if a type is incorrectly specified.
	def impureCompiler(n: Int): ArrayA ~> Id  =
	  new (ArrayA ~> Id) {

	    // val arr = Array.fill(5)(-1)
	    val arr = new Array[Any](n)
	    arr(1) = 0
	    arr(2) = 0


	    def apply[A](fa: ArrayA[A]): Id[A] =
	      fa match {
	        case Put(key, value) =>
	          println(s"put($key, $value)")
	          arr(key) = value
	          ()
	        case Get(key) =>
	          println(s"get($key)")
	          Option(arr(key).asInstanceOf[A])
	      }
	  }

	def execRes(n:Int, i: Int) = {
		val result: Option[Int] = program(i).foldMap(impureCompiler(n))
		println(result)
	}

   // val names = Array("chris", "ed", "maurice")

	// val userBase = List(User("Travis", 28),
 //      User("Kelly", 33),
 //      User("Jennifer", 44),
 //      User("Dennis", 23))

 //    val twentySomethings = for { //( is not used when definitions are present
 //      user <- userBase //Generators
 //      age = user.age //Definitions
 //      if age >= 20 && age < 30 //Filters
 //    }
 //      yield user.name  // i.e. add this to a list

 //    //Compiler converts it to following
 //    //val twentySomethings = userBase.
 //    // withFilter(((user) => user.age.$greater$eq(20).
 //    // $amp$amp(user.age.$less(30)))).
 //    // map(((user) => user.name));

	case class LongProduct(value: Long)
	implicit val longProdMonoid: Monoid[LongProduct] = new Monoid[LongProduct] {
	  def empty: LongProduct = LongProduct(1)
	  def combine(x: LongProduct, y: LongProduct): LongProduct = LongProduct(x.value * y.value)
	}
	def powWriter(x: Long, exp: Long): Writer[LongProduct, Unit] =
	  exp match {
	    case 0 => Writer(LongProduct(1L), ())
	    case m =>
	      Writer(LongProduct(x), ()) >>= { _ => powWriter(x, exp - 1) }
	  }	  


}
