package com.sa.common.ctexm

/*
	https://typelevel.org/cats/datatypes/freemonad.html#what-is-free-in-theory

	Free monads provide a practical way to 
		- represent stateful computations as data and run them
		- run recursive computations in a stack-safe way
		- build an embedded DSL
		- retarget a computation to another interpreter using natural transformations

	https://efekahraman.github.io/2019/03/an-example-of-free-monads-and-optimization

	https://scalac.io/free-monad-cats-overview/

	https://www.slideshare.net/hermannhueck/composing-an-app-with-free-monads-using-cats

	https://softwaremill.com/free-monads/

	https://bartoszmilewski.com/2018/08/20/recursion-schemes-for-higher-algebras/
		
*/
object FreeMonad extends App{

	/*

		the computation to be represented as a pure, immutable value
		to separate the creation and execution of the program
		to be able to support many different methods of execution

	*/

	/*
		Steps for using Free Monad to create an embedded DSL

		1. Study the topic:
			For example for a key value store do three things with keys
				- put value into a store associated with the key
				- get the value from the store given its key
				- delete the a value given its key

		2. But the GOAL is
			- to represent computation as a pure immutable value
			- to separate creation and execution of a program
			- to be able to support many different methods of execution

		3. Study the Grammar:
			Three commands to interact with key value store
				- Put 
				- Get
				- Delete
	*/
	/*
		4. Create Algebraic Data Types (ADT)
			- Closed set of types
			- can be combined to build up complex, recursive values
	*/	
	sealed trait KVStoreA[A]
	case class Put[T] (key: String, value: T) extends KVStoreA[Unit]
	case class Get[T] (key: String) extends KVStoreA[Option[T]]
	case class Delete[T] (key: String) extends KVStoreA[Unit]


/*
	There are five basic steps to “freeing” the ADT:

		Create a type based on Free[_] and KVStoreA[_].


		*/

	import cats.free.Free
	
	type KVStore[A] = Free[KVStoreA, A]

	/*			
		Create smart constructors for KVStore[_] using liftF.
	*/
	import cats.free.Free.liftF

	// put returns nothing (Unit)
	def put[T](key: String, value: T): KVStore[Unit] = 
		liftF[KVStoreA,Unit](Put[T](key,value))

	// get returns a T value
	def get[T](key: String): KVStore[Option[T]] =
		liftF[KVStoreA, Option[T]](Get[T](key))

	// delete returns nothing (Unit)
	def delete[T](key: String): KVStore[Unit] =
		liftF(Delete(key))

	// update composes get and set and returns nothing
	def update[T](key: String, f: T => T): KVStore[Unit] = 
		for {
			vMaybe <- get[T](key)
			_ <- vMaybe.map(v => put[T](key, f(v))).getOrElse(Free.pure(()))
		} yield ()

	// Build a program out of key-value DSL operations.

	/*
		Free[_] is used to create an embedded DSL.
		DSL only represents a sequence of operations defined by a recursive data structure.
		It doesn't produce anything

		Free[_] is programming language inside a programming language
	*/

	def program: KVStore[Option[Int]] = 
		for {
			_ <- put("wild-cats", 2)
			_ <- update[Int]("wild-cats", _ + 12)
			_ <- put("tame-cats", 5)
			n <- get[Int]("wild-cats")
			_ <- delete("tame-cats")
		} yield n

	// Build a compiler for programs of DSL operations.
	/*
		We need to compile our abstract language to effective language and run it.
		We use Natural transformations between type containers.
		Natural Transformations go between types like F[_] and G[_]

		This Natural Transformation 
			is written as FunctionK[F,G]
			or alternatively as F ~> G
		Using simple mutable map as our key value store
	*/
	import cats.arrow.FunctionK
	import cats.{Id, ~>}
	import scala.collection.mutable
/*
	The whole purpose of functional programming isn’t to prevent side-effects, 
	it is just to push side-effects to the boundaries of your system 
	in a well-known and controlled way.
*/	
	def impureCompiler: KVStoreA ~> Id = 
		new (KVStoreA ~> Id){
			val kvs = mutable.Map.empty[String,Any]

			def apply[A](fa: KVStoreA[A]): Id[A] = 
				fa match {
					case Put(key,value) => 
						println(s"put(${key},${value})")
						kvs(key) = value
						()
					case Get(key) => 
						println(s"get(${key})")
						kvs.get(key).map(_.asInstanceOf[A])
					case Delete(key) => 
						println(s"delete(${key})")
						kvs.remove(key)
						()
				}
		}
	// Execute our compiled program.
	/*
		Free[_] is just a recursive structure, that can be seen as
			sequence of operations producing other operations
		The idea behind Free[_] is, that, we fold recursive structure by
			- consuming each operation
			- compiling the operation into our effective language using impure compiler
			- computing next operation
			- continue recursively until reaching a pure state and returning it.
	*/
	// final def foldMap[M[_]](f: FunctionK[S,M])(M: Monad[M]): M[A] = ...
	// M must be a Monad to be flattenable (the famous monoid aspect under Monad). 
	// As Id is a Monad, we can use foldMap.

	def execRes = {
		val result: Option[Int] = program.foldMap(impureCompiler)
		println(result)
	}

	import cats.data.State

	type KVStoreState[A] = State[Map[String, Any], A]
	
	val pureCompiler: KVStoreA ~> KVStoreState = new (KVStoreA ~> KVStoreState) {
	  def apply[A](fa: KVStoreA[A]): KVStoreState[A] =
	    fa match {
	      case Put(key, value) => State.modify(_.updated(key, value))
	      case Get(key) =>
	        State.inspect(_.get(key).map(_.asInstanceOf[A]))
	      case Delete(key) => State.modify(_ - key)
	    }
	}	

	def program2: KVStore[Option[Int]] = 
		for {
			_ <- put("wild-cats", 2)
			_ <- update[Int]("wild-cats", _ + 12)
			_ <- put("tame-cats", 5)
			n <- get[Int]("wild-cats")
			_ <- delete("tame-cats")
		} yield n

	def execPureRes = {
		// val result: (Map[String, Any], Option[Int]) = program.foldMap(pureCompiler).run(Map.empty).value
		val result = program2.foldMap(pureCompiler).run(Map.empty)
		// println(result)
		result
	}

// countDerangementsTD n = resArr Data.Array.! n where
//   resArr = runSTArray $ do
//       arr <- MArray.newArray (1, n) (toInteger (-1))
//       writeArray arr 1 (toInteger 0)
//       writeArray arr 2 (toInteger 1)
//       cdRecursiveTD n arr
//       return arr
      
// -- cdRecursiveTD :: (MArray a b m, Ix b, Num b) => b -> a b b -> m b
// cdRecursiveTD i stArr = do
//     v  <- readArray stArr i
//     when (v == -1) $ do
//       iMinusOne <- cdRecursiveTD (i-1) stArr
//       iMinusTwo <- cdRecursiveTD (i-2) stArr
//       writeArray stArr i ( (i-1) * ( iMinusOne + iMinusTwo )  )
//     readArray stArr i


	sealed trait VecA[A]
	case class PutV[T] (idx: Int, value: T) extends VecA[Unit]
	case class GetV[T] (idx: Int) extends VecA[Option[T]]

}
