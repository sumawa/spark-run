package com.sa.common.ctexm

/*
	Monadic mutation using Free Monad
	
	https://typelevel.org/cats/datatypes/freemonad.html
	https://apocalisp.wordpress.com/2011/12/19/towards-an-effect-system-in-scala-part-2-io-monad/
	https://apocalisp.wordpress.com/2011/03/20/towards-an-effect-system-in-scala-part-1/

	scalaz example
	https://stackoverflow.com/questions/19657022/when-to-use-the-st-monad-in-scala

	Useful
	https://gvolpe.github.io/talks/

	https://functional.works-hub.com/jobs/software-engineer-scala-a76
	
*/

object Effect extends App{

}
