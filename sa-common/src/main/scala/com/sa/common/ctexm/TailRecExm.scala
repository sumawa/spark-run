package com.sa.common.ctexm

// import cats._, cats.data._, cats.implicits._

import cats.Monoid			// for Monoid
import cats.FlatMap
import cats.data.Writer		// for Writer
import cats.implicits._ 	// for >>=

// MUST https://www.lihaoyi.com/post/TheDeathofHypeWhatsNextforScala.html

// MUST https://www.youtube.com/watch?v=LhGq4HlozV4
// https://softwaremill.com/free-monads/
// https://efekahraman.github.io/2019/03/an-example-of-free-monads-and-optimization

// ABOUT COPRODUCT AND INJECT
// https://underscore.io/blog/posts/2017/03/29/free-inject.html

object TailRecExm extends App{

		case class LongProduct(value: Long)

	def exec1(i:Int, j: Int) = {
		implicit val longProdMonoid: Monoid[LongProduct] = new Monoid[LongProduct] {
		  def empty: LongProduct = LongProduct(1)
		  def combine(x: LongProduct, y: LongProduct): LongProduct = LongProduct(x.value * y.value)
		}
		def powWriter(x: Long, exp: Long): Writer[LongProduct, Unit] =
		  exp match {
		    case 0 => Writer(LongProduct(1L), ())
		    case m =>
		      Writer(LongProduct(x), ()) >>= { _ => powWriter(x, exp - 1) }
		  }	  

	   powWriter(i,j)

	}

	def exec2(i:Int, j:Int) = {

		implicit val longProdMonoid: Monoid[LongProduct] = new Monoid[LongProduct] {
		  def empty: LongProduct = LongProduct(1)
		  def combine(x: LongProduct, y: LongProduct): LongProduct = LongProduct(x.value * y.value)
		}

		def powWriter2(x: Long, exp: Long): Writer[LongProduct, Unit] =
		  FlatMap[Writer[LongProduct, ?]].tailRecM(exp) {
		    case 0L      => Writer.value[LongProduct, Either[Long, Unit]](Right(()))
		    case m: Long => Writer.tell(LongProduct(x)) >>= 
		    						{ _ => Writer.value(Left(m - 1)) }
		  }

	   powWriter2(i,j)
	}


	case class BigIntProduct(value: BigInt)

	def exec3(i:Int, j:Int) = {
		implicit val bigIntProdMonoid: Monoid[BigIntProduct] = 
			new Monoid[BigIntProduct]{
				def empty: BigIntProduct = BigIntProduct(1)
				def combine(x: BigIntProduct, y: BigIntProduct): BigIntProduct =
					BigIntProduct(x.value * y.value)
			}
		val bigIntZero = BigInt(0)
		def powWriterBigInt(x: BigInt, exp: BigInt): Writer[BigIntProduct, Unit] =
			FlatMap[Writer[BigIntProduct, ?]].tailRecM(exp){
				case `bigIntZero` => Writer.value[BigIntProduct, Either[BigInt,Unit]](Right(()))
				case m: BigInt => Writer.tell(BigIntProduct(x)) >>=
					{ _ => Writer.value(Left(m-1))}


			}
		powWriterBigInt(i,j)
	}

}
