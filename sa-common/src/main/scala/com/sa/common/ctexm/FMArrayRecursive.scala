package com.sa.common.ctexm

import cats._
import cats.data._
import cats.implicits._

// VERY GOOD FreeMonad and natural transformations
// https://www.slideshare.net/hermannhueck/composing-an-app-with-free-monads-using-cats

// https://softwaremill.com/free-monads/

// MUST https://www.lihaoyi.com/post/TheDeathofHypeWhatsNextforScala.html

// MUST https://www.youtube.com/watch?v=LhGq4HlozV4
// https://softwaremill.com/free-monads/
// https://efekahraman.github.io/2019/03/an-example-of-free-monads-and-optimization

// https://github.com/pvillega/free-monad-sample

object FMArrayRecursive extends App{

	sealed trait ArrayA[A]
	case class Put[T](idx: Int, value: T) extends ArrayA[Unit]
	case class Get[T](idx: Int) extends ArrayA[Option[T]]
	case class CheckAndGet[T](idx: Int, f: Int => Boolean) extends ArrayA[Option[T]]


	import cats.free.Free

	type ArrayStore[A] = Free[ArrayA, A]

	import cats.free.Free.liftF

	// Put returns nothing (i.e. Unit).
	def put[T](idx: Int, value: T): ArrayStore[Unit] =
	  liftF[ArrayA, Unit](Put[T](idx, value))

	// Get returns a T value.
	def get[T](idx: Int): ArrayStore[Option[T]] =
	  liftF[ArrayA, Option[T]](Get[T](idx))

	def checkAndGet[T](idx: Int, f: Int => Boolean): ArrayStore[Option[T]] =
	  liftF[ArrayA, Option[T]](CheckAndGet[T](idx, f))

	def program(i:Int): ArrayStore[Option[Int]] =
		for {
				_ <- put[Int](1, 0)
				_ <- put[Int](2, 1)
				// if (arr(3) == -1) put[Int](3,11) 
				// n <- get[Int](i) 
				n <- checkAndGet[Int](i, (x: Int) => (x == -1)) 
			} yield n

	def cdRecursive(i: Int): ArrayStore[Option[Int]] = 
		for {
			v <- get[Int](i)
			iMinusOne <- if (v == -1) cdRecursive(i-1) else get[Int](2)
			iMinusTwo <- if (v == -1) cdRecursive(i-2) else get[Int](1)
			_ <- put[Int](i, (i-1) * (iMinusOne.get + iMinusTwo.get))
			// _ <- put[Int](i, iMinusOne.get)
			nV <- get[Int](i)
		} yield nV

	import cats.arrow.FunctionK
	import cats.{Id, ~>}
	import scala.collection.mutable

	// the program will crash if a key is not found,
	// or if a type is incorrectly specified.
	def impureCompiler(n: Int): ArrayA ~> Id  =
	  new (ArrayA ~> Id) {

	    // val arr = Array.fill(5)(-1)
	    val arr = new Array[Any](n)
	    arr(1) = 0
	    arr(2) = 1
	    (3 to n-1) map (i => arr(i) = -1)


	    def apply[A](fa: ArrayA[A]): Id[A] =
	      fa match {
	        case Put(key, value) =>
	          println(s"put($key, $value)")
	          arr(key) = value
	          ()
	        case Get(key) =>
	          println(s"get($key)")
	          Option(arr(key).asInstanceOf[A])
	        case CheckAndGet(key,f) =>
	          println(s"checkAndGet($key)")
	          val v = arr(key).asInstanceOf[A]
	          println(s"v.isInstanceOf[Int]: ${v.isInstanceOf[Int]}")
	          if (v.isInstanceOf[Int]){
	          	val updatedVal = if (f(v.asInstanceOf[Int])){
	          		val iMinusOne = arr(key-1)
	          		val iMinusTwo = arr(key-2)
	          		arr(key) = (key.asInstanceOf[Int]-1) * (iMinusOne.asInstanceOf[Int] + iMinusTwo.asInstanceOf[Int])
	          		println(s"iMinusOne.asInstanceOf[Int]: ${arr(key)}")
	          		arr(key)
	          	} else arr(key)
	          	Option(updatedVal.asInstanceOf[A])
	          }
	          else 
	          	Option(arr(key).asInstanceOf[A])

	      }
	  }

	def execRes(n:Int, i: Int) = {
		val result: Option[Int] = program(i).foldMap(impureCompiler(n))
		println(result)
	}

	def execRecursive(n:Int, i: Int) = {
		val result: Option[Int] = cdRecursive(i).foldMap(impureCompiler(n))
		println(result)
	}

	case class LongProduct(value: Long)

	def exec1(i:Int, j: Int) = {
		implicit val longProdMonoid: Monoid[LongProduct] = new Monoid[LongProduct] {
		  def empty: LongProduct = LongProduct(1)
		  def combine(x: LongProduct, y: LongProduct): LongProduct = LongProduct(x.value * y.value)
		}
		def powWriter(x: Long, exp: Long): Writer[LongProduct, Unit] =
		  exp match {
		    case 0 => Writer(LongProduct(1L), ())
		    case m =>
		      Writer(LongProduct(x), ()) >>= { _ => powWriter(x, exp - 1) }
		  }	  

	   powWriter(i,j)

	}

	def exec2(i:Int, j:Int) = {

		implicit val longProdMonoid: Monoid[LongProduct] = new Monoid[LongProduct] {
		  def empty: LongProduct = LongProduct(1)
		  def combine(x: LongProduct, y: LongProduct): LongProduct = LongProduct(x.value * y.value)
		}

		def powWriter2(x: Long, exp: Long): Writer[LongProduct, Unit] =
		  FlatMap[Writer[LongProduct, ?]].tailRecM(exp) {
		    case 0L      => Writer.value[LongProduct, Either[Long, Unit]](Right(()))
		    case m: Long => Writer.tell(LongProduct(x)) >>= { _ => Writer.value(Left(m - 1)) }
		  }

	   powWriter2(i,j)
	}


}
